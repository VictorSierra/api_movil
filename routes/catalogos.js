'use strict'

let express = require('express');
let api = express.Router();
let md_auth = require('../middlewares/autenticacion');
let CatalogControllers=require('../controllers/envioCasillas.controller');

api.post('/obtener-eleccion',md_auth.ensureAuth, CatalogControllers.catalogoEleccion);
api.post('/obtener-municipios',md_auth.ensureAuth,CatalogControllers.catalogoMunicipios);
api.post('/obtener-distritos',md_auth.ensureAuth, CatalogControllers.catalogoDistritos);
api.post('/obtener-secciones',md_auth.ensureAuth,CatalogControllers.catalogoSecciones);
api.post('/obtener-tipoelecciones',md_auth.ensureAuth,CatalogControllers.catalogoTiposEleccions);
api.post('/obtener-casillas',md_auth.ensureAuth,CatalogControllers.catalogoCasillas);
api.post('/sincronizacionfinalizada',md_auth.ensureAuth,CatalogControllers.notificacionSincronicacion);
api.post('/obtener-identificacion-tipoactas',md_auth.ensureAuth, CatalogControllers.catalogoIdentificacionActas);
api.post('/obtener-casillasusuarios',md_auth.ensureAuth, CatalogControllers.obtenerCasillausuarios)
module.exports=api;