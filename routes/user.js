'use strict'
//API MOVIL
let express = require('express');
let UsuariosApp_Controller = require('../controllers/usuariosApp.controller');
let api = express.Router();
let md_auth = require('../middlewares/autenticacion');
let multipart = require('connect-multiparty');
let md_upload = multipart({ uploadDir: './uploads' });
let RecepcionActasController = require('../controllers/recepcionactas.controller');
let ActasController = require('../controllers/actas.controller');

//API SIEE
let ConfigIniCtrl = require('../controllers/user');
// let md_auth = require('../middlewares/autenticacion');
const user = require('../models/user');
//API MOVIL
api.post('/login-user', UsuariosApp_Controller.login_Usuario_android);
api.post('/prep-activacion', UsuariosApp_Controller.prep_activacion);
api.post('/guardar_informacion_android', UsuariosApp_Controller.guardar_informacion_de_android);
api.post('/consultar_informacion_android', UsuariosApp_Controller.consultar_informacion_android);
api.post('/guardar_informacion_de_movil_usuario', UsuariosApp_Controller.guardar_informacion_de_movil_usuario);
api.post('/actualiza-usuarios_app', UsuariosApp_Controller.actualiza_informacion_de_movil_usuario);
api.post('/consulta-usuarios_app', UsuariosApp_Controller.consultar_usuarios_app);
api.post('/generar_usuarios_movil_de_forma_general', UsuariosApp_Controller.generar_usuarios_movil_de_forma_general);
api.post('/uploadActa', [md_auth.ensureAuth, md_upload], RecepcionActasController.agregarActa);
api.post('/uploadActasin',[md_auth.ensureAuth,md_upload],RecepcionActasController.agregarsinqr);
api.post('/reenvio_de_actas_al_siee',[md_upload],RecepcionActasController.reenvio_de_actas_al_siee);
api.get('/archivo/:imagefile', RecepcionActasController.obtenerImagen);
//API SIEE
//api.post('/login-user', ConfigIniCtrl.login_usuario_siee);
api.post('/altausuario_movil', ConfigIniCtrl.alta_usuario_movil);
api.post('/eliminarsuario_movil', ConfigIniCtrl.eliminar_usuario_movil);
api.post('/folios-activacion', ConfigIniCtrl.prep_ingreso_de_folios_de_activacion);
api.post('/desactivar-folios', ConfigIniCtrl.desactivar_folios_de_activacion);
api.post('/consulta-folios', ConfigIniCtrl.consultar_folios_de_activacion);
api.post('/guardar_folios_de_activacion', ConfigIniCtrl.guardar_folios_de_activacion);
api.post('/actualiza_folios_de_activacion', ConfigIniCtrl.actualiza_folios_de_activacion);

//OTRAS FUNCIONES DEL SIEE
api.post('/altaUsuario', ConfigIniCtrl.altaUsuario);
api.post('/altaUsuarioIni', ConfigIniCtrl.altaUsuarioIni);



api.get('/actas', ActasController.obtenerActas);
api.get('/status', UsuariosApp_Controller.status);




module.exports = api;




