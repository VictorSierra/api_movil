FROM node:carbon
WORKDIR /usr/src/app 

RUN apt-get install git-core 
RUN git clone http://192.168.11.82:7500/Juan/API_PREPCasilla_Internet.git
WORKDIR /usr/src/app/API_PREPCasilla_Internet
RUN mkdir uploads
RUN npm install
CMD [ "npm", "start" ]