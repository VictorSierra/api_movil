'use strict'
let LogApp=require('../models/regisitroaccesos.model');
let registroActas=require('../models/Eleccion/RegistrollegadaActa.model');
let BitacoraregistroActas_model =require('../models/Eleccion/BitacoraRegistrollegadaActa.model');
let APPActivacion = require('../models/activacion.model');
let Movil_usuario_model =require('../models/Movil/movil_usuario.model');
let Movil_informacion_model =require('../models/Movil/movil_informacion.model');

//registro de operacion del usuario
function registra(horaAccion ,idUsuario,Aplicacion,nombreUsuario, operacion, parametros, resultados) {
    console.log("registro");
    let registro=new LogApp()
    registro.horaRegistro=horaAccion;
    registro.idUsuario=idUsuario;
    registro.Aplicacion=Aplicacion;
    registro.NombreUsuario=nombreUsuario;
    registro.operacion=operacion;
    registro.parametros=parametros;
    registro.resultados=resultados;
    registro.save((error,registrado)=>{
     if(error){     
         console.log(error);
        return error;
     }else{         
        console.log(registrado);
          return registrado;
     }
    });
}

// //registro de actas que llega a prepcasilla
// function registraActa(dato){    
//     let regacta=new registroActas();
//     regacta.idUsuario=dato.Usuario_ID,
//     regacta.Usuario_ID=dato.Usuario_ID,
//     regacta.Eleccion_ID=dato.Eleccion_ID,
//     regacta.Tipo_Eleccion_ID=dato.Tipo_Eleccion_ID,
//     regacta.Proceso_Electoral=dato.Proceso_Electoral,                                    
//     regacta.horaProcesadoServidor=dato.horaProcesadoServidor,
//     regacta.horacaptura=dato.horacaptura,
//     regacta.hash=dato.hash,
//     regacta.qr=dato.qr,                                
//     regacta.nombreArchivoRepositorio=dato.nombreArchivoRepositorio,
//     regacta.ArchivoOrigen=dato.ArchivoOrigen  
//     regacta.save((error,reg)=>{
//         if(error){
//             console.log(error);
//             return error;
//         }
//         else{
//             //console.log()
//             return reg;
//         }
//     }); 
// }

//registro de actas que llega a prepcasilla
function registraActa(dato){    
    let regacta=new registroActas();

    regacta.Eleccion=dato.Eleccion;
    regacta.Ambito=dato.Ambito;
    regacta.Seccion=dato.Seccion;
    regacta.Casilla=dato.Casilla;
    regacta.QRActa=dato.CodigoQR;
    regacta.ProcesoElectoral=dato.Proceso_Electoral;
    regacta.HoraProcesadoServidor=dato.HoraProcesadoServidor;
    regacta.HoraCaptura=dato.HoraCaptura;
    regacta.FechaAcopio=dato.FechaAcopio;
    regacta.HoraAcopio=dato.HoraAcopio;
    regacta.Usuario=dato.Usuario;
    regacta.Hash=dato.Hash;
    regacta.NombreArchivoRepositorio=dato.NombreArchivoRepositorio;
    regacta.ArchivoOrigen=dato.ArchivoOrigen;
    regacta.Ubicacion=dato.Ubicacion;
    regacta.Imei=dato.Imei;
    regacta.EnviadoCorrectamente=dato.enviadoCorrectamente;
    regacta.MsgError=dato.error;
    regacta.ApiRecibe=dato.ApiRecibe;
    regacta.CasillaNombre=dato.CasillaNombre;
    regacta.EleccionNombre=dato.EleccionNombre;  
    regacta.AmbitoNombre=dato.AmbitoNombre;  
    regacta.Fecharegistro = new Date();  


    console.log("regacta => ", regacta);
    
    // guardar el status donde diga si se guardó 
    // guardar el status donde diga si no se guardó
    // meterlo en el response en recepcon actas
    // guardar el imei y ubicacion
    regacta.save((error,reg)=>{
        if(error){
            console.log(error);
            return error;
        }
        else{
            //console.log()

            if(dato.enviadoCorrectamente == false)
            {
                // var entidad_bitacora = {
                                                
                //     RegistroRecepcionActas_ID : reg._id,
                //     FechaRegistro : dato.FechaRegistro,
                //     EnviadoCorrectamente : dato.enviadoCorrectamente,
                //     MsgError : dato.error 
                                                          
                // };

                let bitacoraregistroActas_model =new BitacoraregistroActas_model ();

                bitacoraregistroActas_model.RegistroRecepcionActas_ID = reg._id;
                bitacoraregistroActas_model.FechaRegistro = dato.FechaRegistro;
                bitacoraregistroActas_model.EnviadoCorrectamente = dato.EnviadoCorrectamente;
                bitacoraregistroActas_model.MsgError = dato.error;

                bitacoraregistroActas_model.save((error,bitacoraregistroActas_model )=>{
                if(error){     
                    console.log(error);
                    //return error;
                }else{         
                    console.log(bitacoraregistroActas_model);
                    //return bitacoraregistroActas_model;
                }
                });
            }

            return reg;
        }
    }); 
}

//registro de activacion
function activacion_registro(entidad_Activacion) {
    console.log("Activacion");
    let activacion_model =new APPActivacion()
    
    activacion_model.activacion_folio = entidad_Activacion.activacion_folio;
    activacion_model.activacion_rango = entidad_Activacion.activacion_rango;
    activacion_model.activacion_numero_actual = entidad_Activacion.activacion_numero_actual;
    activacion_model.activacion_numero_maxima_permitido = entidad_Activacion. activacion_numero_maxima_permitido;
    activacion_model.activacion_fecha_creacion = entidad_Activacion.activacion_fecha_creacion;
    activacion_model.activacion_activo = entidad_Activacion.activacion_activo;
    activacion_model.save((error,activacion_model)=>{
     if(error){     
         console.log(error);
        return error;
     }else{         
        console.log(activacion_model);
          return activacion_model;
     }
    });
}

function guardar_movil_informacion(entidad_moviles) {
    console.log("guardar_movil_informacion");
    let movil_informacion_model =new Movil_informacion_model()

    movil_informacion_model.movil_informacion_mac = entidad_moviles.movil_informacion_mac;
    movil_informacion_model.movil_informacion_imei = entidad_moviles.movil_informacion_imei;
    movil_informacion_model.movil_informacion_Android_ID = entidad_moviles.movil_informacion_Android_ID;
    movil_informacion_model.movil_informacion_no_de_inventario = entidad_moviles.movil_informacion_no_de_inventario;
    movil_informacion_model.movil_informacion_Origen = entidad_moviles.movil_informacion_Origen;
    movil_informacion_model.movil_informacion_Fecha_Registro = entidad_moviles.movil_informacion_Fecha_Registro;
    movil_informacion_model.movil_informacion_activo = entidad_moviles.movil_informacion_activo;    

    movil_informacion_model.save((error,movil_informacion_model)=>{
     if(error){     
         console.log(error);
        return error;
     }else{         
        console.log(movil_informacion_model);
          return movil_informacion_model;
     }
    });
}

function bitacora_registro_acta_guardar(entidad_bitacora_registro) {
    console.log("bitacora_registro_acta_guardar");
    let bitacoraregistroActas_model =new BitacoraregistroActas_model ();

    bitacoraregistroActas_model.RegistroRecepcionActas_ID = entidad_bitacora_registro.RegistroRecepcionActas_ID;
    bitacoraregistroActas_model.FechaRegistro = entidad_bitacora_registro.FechaRegistro;
    bitacoraregistroActas_model.EnviadoCorrectamente = entidad_bitacora_registro.EnviadoCorrectamente;
    bitacoraregistroActas_model.MsgError = entidad_bitacora_registro.MsgError;

    bitacoraregistroActas_model .save((error,bitacoraregistroActas_model )=>{
     if(error){     
        console.log(error);
        return error;
     }else{         
        console.log(bitacoraregistroActas_model);
        return bitacoraregistroActas_model;
     }
    });
}

function guardar_movil_usuario(entidad_moviles) {
    console.log("guardar_movil_usario");
    let movil_usuario_model =new Movil_usuario_model()

    movil_usuario_model.movil_usuario_Nombre = entidad_moviles.movil_usuario_Nombre;
    movil_usuario_model.movil_usuario_Contrasenia = entidad_moviles.movil_usuario_Contrasenia;
    movil_usuario_model.movil_usuario_Movil = entidad_moviles.movil_usuario_Movil;
    movil_usuario_model.movil_usuario_CATD = entidad_moviles.movil_usuario_CATD;
    movil_usuario_model.movil_usuario_Consecutivo = entidad_moviles.movil_usuario_Consecutivo;
    movil_usuario_model.movil_usuario_Errores = entidad_moviles.movil_usuario_Errores;
    movil_usuario_model.movil_usuario_Fecha_Registro = entidad_moviles.movil_usuario_Fecha_Registro;
    movil_usuario_model.movil_usuario_Activo = entidad_moviles.movil_usuario_Activo;    

    movil_usuario_model.save((error,movil_usuario_model)=>{
     if(error){     
         console.log(error);
        return error;
     }else{         
        console.log(movil_usuario_model);
          return movil_usuario_model;
     }
    });
}

module.exports={
    registra,
    registraActa,
    activacion_registro,
    guardar_movil_informacion,
    bitacora_registro_acta_guardar,
    guardar_movil_usuario
}
