'use strict'
let request = require('request-promise');
let Global = require('../global')
let registros = require('../utilidades/registros');
var registrollegadaActa_model = require('../models/Eleccion/RegistrollegadaActa.model');
let fs = require('fs');

let path = require('path');
const image2base64 = require('image-to-base64');
const { error, log } = require('console');
const moment = require('moment');



const canal = 'ActasEnviadas';
const canalsqr = 'Actassinqr';
//se agregan actas que usaron el QR para digitalizar
function agregarActa(req, res) {
    let params = req;
    let body = req.body;
    console.log(DatosActa)
    let horacaptura = body.horacaptura;
    let hash = body.hash;
    let qr = body.qr;
    let files = params.files;
    let nombreArchivoOrigen = files.imagen.originalFilename;
    let patt = files.imagen.path;
    let DatosActa = params.DatosActa;
    //registros.registra(new Date(),req.user._id,'PREPCASILLA',req.user.nombre,'Recepcion Archivo',JSON.stringify(params),'Llegando archivo ' + files.imagen.originalFilename);
    let file_name = 'sin imagen';
    if (patt) {
        let file_path = patt;
        let file_split = file_path.split('\\');
        file_name = file_split[1];
        let ext_split = file_name.split('.');
        let file_ext = ext_split[1];
        if (file_ext == 'png' || file_ext == 'PNG' || file_ext == 'jpg' || file_ext == 'JPG') {

            fs.stat(patt, (err, stats) => {
                if (err) {

                } else {
                    if (stats.isFile()) {
                        var formData = {
                            imagen: fs.createReadStream(patt)
                        };


                        request.post({ url: Global.Global.urlcargaarchivo, formData: formData }).then(function (htmlstring) {
                            fs.unlink(patt, (error) => {
                                if (error) console.log(error);
                                //console.log('Archivo borrado ' + patt);
                                // registros.registra(new Date(),req.user._id,'PREPCASILLA',user.nombre,'Envio de archivo a repositorio',JSON.stringify(params),'Archivo enviado' + files.imagen.originalFilename);
                            });
                            console.log(htmlstring);
                            var pp = JSON.parse(htmlstring);
                            var datosenviosiee = {
                                Usuario_ID: req.user.sid,
                                horaProcesadoServidor: new Date(),
                                horacaptura: horacaptura,
                                hash: hash,
                                qr: qr,
                                nombreArchivoRepositorio: pp.nombreArchivo,
                                ArchivoOrigen: nombreArchivoOrigen
                            };

                            console.log(pp.nombreArchivo);
                            //console.log(nombreArchivoOrigen);
                            //aqui se notifica a la parte de la api que apunta al siee que existe un acta
                            //registros.registra(new Date(),user._id,"PREPCASILLA",user.nombre,"Recepcion Archivo",JSON.stringify(params),"Envio del acta");
                            //console.log(datosenviosiee);




                            return res.status(200).send({
                                message: 'Archivo Recibido',
                                hash: pp.hash,
                                qr: pp.qr,
                                nombreArchivo: pp.nombreArchivo,
                                ArchivoOrigen: nombreArchivoOrigen
                            });
                        }).catch(function (error) {
                            console.log(error);
                        });
                    }
                }
            });
            //dar de alta el acta capturada y notificar a la api apuntando al siee que llego un acta y que a la vez notifique al cliente siee de que obtenga el acta recibida                            
        } else {
            //eliminar el archivo que llego
            return res.status(400).send({ message: 'Archivo Rechazado', nombreArchivo: file_name, ArchivoOrigen: nombreArchivoOrigen });
        }
    }
}


async function agregarsinqr(req, res) {

    console.log(req.headers);


    console.log('SIN QR');
    let params = req;
    let body = req.body;
    // console.log("body",body);
    let imagen_datos;
    if (body.image_file_string != '') {
        imagen_datos = decodeBase64Image(body.image_file_string);
        console.log(imagen_datos);
    }

    let patt = imagen_datos.ubicacion_imagen;
    //let DatosActa = params.DatosActa;
    let file_name = 'sin imagen';
    if (patt) {
        let file_path = patt;
        let file_split = file_path.split('\\');
        //file_name = file_split[1];
        file_name = imagen_datos.nombre_de_la_imagen + "." + imagen_datos.extencion;
        //let ext_split = file_name.split('.');
        //let file_ext = ext_split[1];
        let file_ext = imagen_datos.extencion
        if (file_ext == 'png' || file_ext == 'PNG' || file_ext == 'jpg' || file_ext == 'JPG') {

            fs.stat(patt, async (err, stats) => {
                if (err) {

                } else {
                    if (stats.isFile()) {
                        // var formData = {
                        //     imagen: fs.createReadStream(patt)
                        // };

                        //pasarlo al then cuando recibas el estatus de enviado o no enviado
                        //crear una propiedad estatus 
                        try {

                            // image2base64(Global.Global.urlarchivo + "/archivo/" + file_name)
                            //     // image2base64(patt)                            
                            //     .then(async (response) => {
                                    // console.log("img ==>", response);

                                    let fechahora = String(body.proceso_digitalizacion_Fecha_Captura).split(' ');
                                    let fecha = fechahora[0];
                                    let hora = fechahora[1];

                                    var datosenviosiee = {
                                        file: body.image_file_string,

                                        Eleccion: body.proceso_digitalizacion_Eleccion,
                                        Ambito: body.proceso_digitalizacion_Ambito,
                                        Seccion: body.proceso_digitalizacion_Seccion,
                                        Casilla: body.proceso_digitalizacion_Casilla,
                                        CodigoQR: body.proceso_digitalizacion_CodigoQR,
                                        Proceso_Electoral: 'ORD-2024',
                                        HoraProcesadoServidor: new Date(),
                                        HoraCaptura: body.proceso_digitalizacion_Fecha_Captura,
                                        FechaAcopio: fecha,
                                        HoraAcopio: hora,
                                        Usuario: body.catalogo_usuario_Nombre, //body.Usuario_ID,
                                        Hash: body.proceso_digitalizacion_Imagen_Hash,
                                        NombreArchivoRepositorio: file_name,
                                        ArchivoOrigen: body.proceso_digitalizacion_Imagen,
                                        Ubicacion: body.proceso_digitalizacion_Ubicacion,
                                        Imei: body.proceso_digitalizacion_Imei,
                                        ApiRecibe: Global.Global.api_nombre,
                                        CasillaNombre: body.catalogo_casilla_Nombre,
                                        EleccionNombre: body.catalogo_eleccion_Nombre,
                                        AmbitoNombre: body.catalogo_ambito_Descripcion,
                                    };

                                    //console.log("datosenviosiee ==> ", datosenviosiee);

                                    //Imei: datosenviosiee.Imei
                                    // estatus: datosenviosiee.estatus

                                    console.log("Inicia envio de Sie");
                                    var recibio_el_sie_el_acta = false;
                                    var mensaje_tmp = "";
                                    await request.post({
                                        url: Global.Global.urlsieeactas,
                                        //  formData: datosenviosiee
                                        //SE ENVIA A PRUEBASIEE
                                        formData: {
                                            file: body.image_file_string,
                                            Eleccion: datosenviosiee.Eleccion,
                                            Ambito: datosenviosiee.Ambito,
                                            Seccion: datosenviosiee.Seccion,
                                            Casilla: datosenviosiee.Casilla,
                                            ProcesoElectoral: datosenviosiee.Proceso_Electoral,
                                            Fecha_Acopio: fecha,
                                            Hora_Acopio: hora,
                                            Usuario: datosenviosiee.Usuario,
                                            Hash: datosenviosiee.Hash,
                                            QR_Acta: datosenviosiee.CodigoQR,
                                            Ubicacion: datosenviosiee.Ubicacion,
                                            Imei: datosenviosiee.Imei
                                        }

                                    }).then(async function (htmlstring) {
                                        console.log("htmlstring", htmlstring);
                                        let resultado_del_envio = JSON.parse(htmlstring);
                                        // (function(estatus){
                                        //     console.log(estatus);                                            
                                        // })


                                        datosenviosiee.enviadoCorrectamente = resultado_del_envio.success;
                                        datosenviosiee.error = resultado_del_envio.error;

                                        registros.registraActa(datosenviosiee);

                                        if (resultado_del_envio.success == true) {
                                            console.log("El acta se envio correctamente");
                                            //se puede actilizar el registro informando que ya recibio el archivo el siee
                                            //registros.registraActa(notificacion);
                                            recibio_el_sie_el_acta = true;
                                            mensaje_tmp = "El acta se envio correctamente";
                                            // return res.status(200).send({
                                            //     success: true,
                                            //     message: 'Archivo Recibido',
                                            //     hash: body.proceso_digitalizacion_Imagen_Hash,
                                            //     qr: '',
                                            //     nombreArchivo: file_name,
                                            //     ArchivoOrigen: body.proceso_digitalizacion_Imagen
                                            // });


                                        } else {

                                            // (estatus == false);{
                                            console.log("El acta no se envió correctamente");
                                            // }   

                                            recibio_el_sie_el_acta = false;
                                            mensaje_tmp = resultado_del_envio.error;

                                            await registrollegadaActa_model.findOne({
                                                'Hash': body.Hash
                                            }, (err, repcionacta_entidad) => {
                                                if (err) {
                                                    //registros.registra(new Date(), codigo_de_activacion, "PREPCASILLA", codigo_de_activacion, "Desactivar Folios de activacion", JSON.stringify(params), "Error al realizar la peticion de desactivar folios")
                                                    // return res.status(500).send({
                                                    //     activacion : false,
                                                    //     message: 'Error al realizar la consulta del registro del acta '
                                                    // });
                                                } else {
                                                    if (!repcionacta_entidad) {
                                                        //registros.registra(new Date(), codigo_de_activacion, "PREPCASILLA", codigo_de_activacion, "Desactivar Folios de activacion", JSON.stringify(params), "El codigo de activacion es incorrecto");
                                                        // return res.status(400).send({
                                                        //     activacion : false,
                                                        //     message: 'No existe el registro solicitado'
                                                        // });
                                                    } else {

                                                        var entidad_bitacora = {

                                                            RegistroRecepcionActas_ID: repcionacta_entidad._id,
                                                            FechaRegistro: entidad_bitacora_registro.FechaRegistro,
                                                            EnviadoCorrectamente: datosenviosiee.enviadoCorrectamente,
                                                            MsgError: datosenviosiee.error

                                                        };

                                                        registros.bitacora_registro_acta_guardar(entidad_bitacora);

                                                    }
                                                }
                                            });

                                            //console.log("res",res);
                                            console.log("res antes del retur");

                                            // res.status(200).send({
                                            //     success: false,
                                            //     message: 'El acta no se envió correctamente',
                                            //     // hash: body.proceso_digitalizacion_Imagen_Hash,
                                            //     // qr: '',
                                            //     // nombreArchivo: file_name,
                                            //     // ArchivoOrigen: body.proceso_digitalizacion_Imagen,
                                            //     // error: resultado_del_envio.error
                                            // });

                                            //console.log("res despues del retur");

                                        }


                                    }).catch(function (error) {
                                        console.log("error_1", error.error);

                                        datosenviosiee.enviadoCorrectamente = false;
                                        datosenviosiee.error = error.message;

                                        registros.registraActa(datosenviosiee);


                                        recibio_el_sie_el_acta = false;
                                        mensaje_tmp = error.message;


                                        console.log("file_name", file_name);
                                        // return res.status(200).send({
                                        //     success: false,
                                        //     message: 'Error al enviar el acta',
                                        //     hash: body.proceso_digitalizacion_Imagen_Hash,
                                        //     qr: '',
                                        //     nombreArchivo: file_name,
                                        //     ArchivoOrigen: body.proceso_digitalizacion_Imagen,
                                        //     error: error.message
                                        // });

                                        // registrollegadaActa_model.findOne({
                                        //     'Hash': body.Hash
                                        // }, (err, repcionacta_entidad) => {
                                        //     if (err) 
                                        //     {
                                        //         //registros.registra(new Date(), codigo_de_activacion, "PREPCASILLA", codigo_de_activacion, "Desactivar Folios de activacion", JSON.stringify(params), "Error al realizar la peticion de desactivar folios")
                                        //         return res.status(500).send({
                                        //             activacion : false,
                                        //             message: 'Error al realizar la consulta del registro del acta '
                                        //         });
                                        //     } else {
                                        //         if (!repcionacta_entidad) {
                                        //             //registros.registra(new Date(), codigo_de_activacion, "PREPCASILLA", codigo_de_activacion, "Desactivar Folios de activacion", JSON.stringify(params), "El codigo de activacion es incorrecto");
                                        //             return res.status(400).send({
                                        //                 activacion : false,
                                        //                 message: 'No existe el registro solicitado'
                                        //             });
                                        //         } else {

                                        //             var entidad_bitacora = {

                                        //                 RegistroRecepcionActas_ID : repcionacta_entidad._id,
                                        //                 FechaRegistro : entidad_bitacora_registro.FechaRegistro,
                                        //                 EnviadoCorrectamente : datosenviosiee.enviadoCorrectamente,
                                        //                 MsgError : datosenviosiee.error 

                                        //             };

                                        //             registros.bitacora_registro_acta_guardar(entidad_bitacora);

                                        //         }
                                        //     }
                                        // });
                                    });

                                    return res.status(200).send({
                                        success: recibio_el_sie_el_acta,
                                        message: mensaje_tmp,
                                        hash: body.proceso_digitalizacion_Imagen_Hash,
                                        qr: '',
                                        nombreArchivo: file_name,
                                        ArchivoOrigen: body.proceso_digitalizacion_Imagen
                                    });

                                //})
                                // .catch(
                                //     (error) => {
                                //         console.log("e2", error); //Exepection error....
                                //         return res.status(200).send({
                                //             success: false,
                                //             message: 'Error al enviar el acta',
                                //             hash: body.proceso_digitalizacion_Imagen_Hash,
                                //             qr: '',
                                //             nombreArchivo: file_name,
                                //             ArchivoOrigen: body.proceso_digitalizacion_Imagen,
                                //             error: error.message
                                //         });

                                //     }
                                // );

                        } catch (error) {
                            console.log("e3", error);
                            return res.status(200).send({
                                success: false,
                                message: 'Detalles del Error: ' + error,
                                hash: body.proceso_digitalizacion_Imagen_Hash,
                                qr: '',
                                nombreArchivo: file_name,
                                ArchivoOrigen: body.proceso_digitalizacion_Imagen
                            });
                        }


                    }
                }
            });
            //dar de alta el acta capturada y notificar a la api apuntando al siee que llego un acta y que a la vez notifique al cliente siee de que obtenga el acta recibida                            
        } else {
            //eliminar el archivo que llego
            return res.status(400).send({ message: 'Archivo Rechazado', nombreArchivo: file_name, ArchivoOrigen: nombreArchivoOrigen });
        }
    }
}


function reenvio_de_actas_al_siee(req, res) {

    // console.log('Hola Mundo');
    let params = req;
    let body = req.body;

    console.log(req.body);

    //let patt = Global.Global.urlarchivo + "/archivo/" + file_name;
    let patt = "./uploads/" + body.NombreArchivoRepositorio;
    let file_name = body.NombreArchivoRepositorio;


    console.log(patt);
    // return;

    fs.stat(patt, (err, stats) => {
        if (err) {
            console.log("Error: " + err);
        } else {
            if (stats.isFile()) {
                // var formData = {
                //     imagen: fs.createReadStream(patt)
                // };


                //pasarlo al then cuando recibas el estatus de enviado o no enviado
                //crear una propiedad estatus 


                try {
                    image2base64("./uploads/" + body.NombreArchivoRepositorio)
                        //image2base64(Global.Global.urlarchivo + "/archivo/" + file_name)
                        // image2base64(patt)                            
                        .then((response) => {
                            //console.log("img ==>", response);

                            registrollegadaActa_model.findOne({
                                '_id': body._id
                            }, (err, repcionacta_entidad) => {
                                if (err) {
                                    //registros.registra(new Date(), codigo_de_activacion, "PREPCASILLA", codigo_de_activacion, "Desactivar Folios de activacion", JSON.stringify(params), "Error al realizar la peticion de desactivar folios")
                                    return res.status(500).send({
                                        success: false,
                                        message: 'Error al '
                                    });
                                } else {
                                    if (!repcionacta_entidad) {
                                        //registros.registra(new Date(), codigo_de_activacion, "PREPCASILLA", codigo_de_activacion, "Desactivar Folios de activacion", JSON.stringify(params), "El codigo de activacion es incorrecto");
                                        return res.status(400).send({
                                            success: false,
                                            message: 'No existe el registro solicitado'
                                        });
                                    } else {

                                        const fechaMoment = moment(repcionacta_entidad.FechaAcopio);

                                        // let fechahora = String(body.proceso_digitalizacion_Fecha_Captura).split(' ');                                    
                                        // let fecha = fechahora[0];                            let hora = fechahora[1];
                                        request.post({
                                            url: Global.Global.urlsieeactas,
                                            //  formData: datosenviosiee
                                            //SE ENVIA A PRUEBASIEE
                                            formData: {
                                                file: response,
                                                Eleccion: repcionacta_entidad.Eleccion,
                                                Ambito: repcionacta_entidad.Ambito,
                                                Seccion: repcionacta_entidad.Seccion,
                                                Casilla: repcionacta_entidad.Casilla,
                                                ProcesoElectoral: repcionacta_entidad.ProcesoElectoral,
                                                Fecha_Acopio: fechaMoment.format('YYYY-MM-DD'),
                                                Hora_Acopio: repcionacta_entidad.HoraAcopio,
                                                Usuario: repcionacta_entidad.Usuario,
                                                Hash: repcionacta_entidad.Hash,
                                                QR_Acta: repcionacta_entidad.QRActa,
                                                Ubicacion: repcionacta_entidad.Ubicacion,
                                                Imei: repcionacta_entidad.Imei
                                            }

                                        }).then(function (htmlstring) {
                                            console.log("htmlstring");
                                            //console.log(htmlstring);
                                            let resultado_del_envio = JSON.parse(htmlstring);
                                            // (function(estatus){
                                            //     console.log(estatus);                                            
                                            // })


                                            repcionacta_entidad.enviadoCorrectamente = resultado_del_envio.success;
                                            repcionacta_entidad.error = resultado_del_envio.error;


                                            if (resultado_del_envio.success == true) {
                                                console.log("El acta se envio correctamente");
                                                //se puede actilizar el registro informando que ya recibio el archivo el siee
                                                //registros.registraActa(notificacion);



                                            } else {

                                                // (estatus == false);{
                                                console.log("El acta no se envió correctamente");
                                                // }

                                                var entidad_bitacora = {

                                                    RegistroRecepcionActas_ID: repcionacta_entidad._id,
                                                    FechaRegistro: new Date(Date.now()),
                                                    EnviadoCorrectamente: repcionacta_entidad.enviadoCorrectamente,
                                                    MsgError: repcionacta_entidad.error

                                                };

                                                registros.bitacora_registro_acta_guardar(entidad_bitacora);

                                            }

                                            //Actualizamos el numero de activaciones
                                            registrollegadaActa_model.updateOne({ "_id": repcionacta_entidad._id }, {
                                                enviadoCorrectamente: resultado_del_envio.success,
                                                error: resultado_del_envio.error,
                                                // FechaAcopio: repcionacta_entidad.FechaAcopio,
                                                // HoraAcopio: repcionacta_entidad.HoraAcopio

                                            }, (err, usuario) => {

                                                return res.status(200).send({
                                                    success: true,
                                                    message: 'Acta actualizada',
                                                    repcionacta_entidad
                                                });
                                            });

                                            // registros.registraActa(datosenviosiee);


                                        }).catch(function (error) {
                                            console.log("error");
                                            console.log(error);

                                            repcionacta_entidad.enviadoCorrectamente = false;
                                            repcionacta_entidad.error = error.message;

                                            registrollegadaActa_model.updateOne({ "_id": repcionacta_entidad._id }, {
                                                enviadoCorrectamente: repcionacta_entidad.enviadoCorrectamente,
                                                error: repcionacta_entidad.error
                                                // FechaAcopio: repcionacta_entidad.FechaAcopio,
                                                // HoraAcopio: repcionacta_entidad.HoraAcopio

                                            }, (err, usuario) => {
                                                return res.status(400).send({
                                                    success: true,
                                                    message: 'La acta no se logro enviar'
                                                });
                                            });

                                            var entidad_bitacora = {

                                                RegistroRecepcionActas_ID: repcionacta_entidad._id,
                                                FechaRegistro: new Date(Date.now()),
                                                EnviadoCorrectamente: repcionacta_entidad.enviadoCorrectamente,
                                                MsgError: repcionacta_entidad.error

                                            };

                                            registros.bitacora_registro_acta_guardar(entidad_bitacora);

                                            //registros.registraActa(datosenviosiee);
                                            return res.status(400).send({
                                                success: false,
                                                message: 'La acta no se logro enviar'
                                            });

                                        });

                                    }
                                }
                            });

                        })
                        .catch(
                            (error) => {
                                console.log("e2", error); //Exepection error....
                            }
                        );

                } catch (error) {
                    console.log("e3", error);
                    return res.status(200).send({
                        success: false,
                        message: 'Detalles del Error: ' + error,
                        hash: body.proceso_digitalizacion_Imagen_Hash,
                        qr: '',
                        nombreArchivo: file_name,
                        ArchivoOrigen: body.proceso_digitalizacion_Imagen
                    });
                }


            }
        }
    });




}


function obtenerImagen(req, res) {
    console.log("obtenerImagen -> req.params =>", req.params);

    let imagen_file = req.params.imagefile;
    let path_file = './uploads/' + imagen_file;

    if (path_file.length > 0) {
        console.log("obtenerImagen -> path_file =>", path_file);
        fs.stat(path_file, (err, stats) => {
            if (err) {
                return res.status(400).send({ message: 'No existe la imagen' });
            } else {
                if (stats.isFile()) {
                    return res.sendFile(path.resolve(path_file));
                }
            }
        });
    } else {
        return res.status(200).send({ message: 'no tiene una imagen asociada' });
    }

}

function decodeBase64Image(dataString) {

    let file_datos = {};
    let data_buffer = new Buffer.from(dataString, 'base64');

    file_datos.ubicacion_directorio = './uploads';

    var uniqueSHA1String = genera_random_string();

    file_datos.nombre_de_la_imagen = 'image-' + uniqueSHA1String;
    file_datos.extencion = 'jpg';

    file_datos.ubicacion_imagen = file_datos.ubicacion_directorio + "/" +
        file_datos.nombre_de_la_imagen +
        '.' +
        file_datos.extencion;

    try {
        require('fs').writeFile(file_datos.ubicacion_imagen, data_buffer,
            function () {
                console.log('DEBUG - feed:message: Saved to disk image attached by user:', file_datos.ubicacion_imagen);
                //return file_datos;
            });
    } catch (error) {
        console.log('ERROR:', error);
        file_datos.errror = error;
    }

    return file_datos;
}


function genera_random_string() {
    // Generate random string
    var crypto = require('crypto');
    var seed = crypto.randomBytes(20);
    var uniqueSHA1String = crypto
        .createHash('sha1')
        .update(seed)
        .digest('hex');

    return uniqueSHA1String;
}

module.exports = {
    agregarActa,
    agregarsinqr,
    obtenerImagen,
    reenvio_de_actas_al_siee
};