'use strict'
let UsuarioAPP = require('../models/usuarioApp.model');
let Casillas = require('../models/Casillas/casillas.model');
let Actas = require('../models/Actas/actas.model');
let ProcesoElectoral = require('../models/Eleccion/ProcesoElectoral.model');
let Municipios = require('../models/Eleccion/Municipios.model');
let Distritos = require('../models/Eleccion/DistritosElectorales.model');
let Secciones = require('../models/Eleccion/SeccionesElectorales.model');
let TiposEleccion = require('../models/Eleccion/TipoEleccion.model');
let identificacionTipoActas=require('../models/Eleccion/IdentidadTipoActas.models');
let casillasxusuarios=require('../models/Eleccion/CasillasUsuarios.model');
//let md5=require('md5');
function ObtenerCasillasProceso(req, res) {
    let params = req.body;
    let procesoElectoral = params.procesoElectoral;
    let NombreUsuario = params.nombre;
    if (NombreUsuario != null && procesoElectoral != null) {
        UsuarioAPP.findOne({
            nombre: NombreUsuario
        }, (errorB, user) => {
            if (errorB) {
                return res.status(404).send({
                    message: 'Hubo un problema en la peticón de casillas'
                })
            } else {
                if (!user) {
                    return res.status(404).send({
                        message: 'Usuario no encontrado'
                    });
                } else {
                    Casillas.find({
                        ProcesoElectoral: procesoElectoral,
                        UsuarioIdentificado: user.IdUsuario
                    }, (errorC, casillas) => {
                        if (errorC) {
                            return res.status(404).send({
                                message: 'Error al procesar la peticion de casillas'
                            });
                        } else {
                            if (!casillas) {
                                return res.status(404).send({
                                    message: 'Proceso Electoral no encontrado'
                                });
                            } else {
                                //aqui se debera filtrar la información que se manda a los dispositivos moviles y agregar la necesaria para poder operar en los dispositivos moviles.
                                return res.status(200).send({
                                    casillas: casillas
                                });
                            }
                        }
                    });
                }
            }
        });

    } else {
        res.status(404).send({
            message: 'Los datos requerido no estan completos'
        });
    }
}

function Sincronizacion(req, res) {
    let params = req.body;
    let hashcontenido = params.hashcontenido;
    let idUsuario = params.idUsuario;

    if (!hashcontenido) {
        //sincronizacion inicial
        Casillas.find({
            UsuarioAsignado: idUsuario
        }).populate('Actas').exec((error, resultadocasillas) => {
            if (error) {
                return res.status(500).send({
                    message: 'Ocurrio un problema al procesar la petición'
                });
            } else {
                if (!resultadocasillas) {
                    return res.status(404).send({
                        message: 'No se encontraron casillas asignadas'
                    });
                } else {
                    let hash = md5(resultadocasillas);
                    return res.status(200).send({
                        Casillas: resultadocasillas,
                        hashcontenido: hash
                    });
                }
            }
        });
    } else {
        Casillas.find({
            UsuarioAsignado: idUsuario
        }).populate('Actas').exec((error, resultadocasillas) => {
            if (error) {
                return res.status(500).send({
                    message: 'Ocurrio un problema al procesar la petición'
                });
            } else {
                if (!resultadocasillas) {
                    return res.status(404).send({
                        message: 'No se encontraron casillas asignadas'
                    });
                } else {
                    let hash = md5(resultadocasillas);
                    if (hash == hashcontenido) {
                        return res.status(200).send({
                            message: 'No existen cambios en sus configuración'
                        });
                    } else {
                        return res.status(200).send({
                            Casillas: resultadocasillas,
                            hashcontenido: hash
                        });
                    }

                }
            }
        });
    }
}
//Catalogo de identificacion de tipo de actas
function catalogoIdentificacionActas(req,res){
    let params=req.body;
    identificacionTipoActas.find({},(error,tipoactas)=>{
        if(error){
            return res.status(500).send({message:'Error al obtener identificación tipo actas ' +error});
        }else{
            if(!tipoactas){
                return res.status(400).send({message:'No se encontraron identificación tipo actas'});
            }else{
                return res.status(200).send({IdentificacionTipoActas:tipoactas});
            }
        }
    });
}
//Envia el catalogo de actas
function catalogoEleccion(req, res) {
    let params = req.body;
    let idUsuario = params.idUsuario;
    ProcesoElectoral.findOne({
        Activo: true
    }, (error, proceso) => {
        if (error) {
            return res.status(500).send({
                message: 'Error al obtener el proceso ' + error
            });
        } else {
            if (!proceso) {
                return res.status(400).send({
                    message: 'no se encontro el proceso'
                });
            } else {
                console.log(proceso);
                return res.status(200).send({
                    procesoElectoral: proceso
                });
            }
        }
    });
}
//envia el catalogo de municipios
function catalogoMunicipios(req, res) {
    let params = req.body;
    let idUsuario = params.idUsuario;
    Municipios.find({
        Activo: true
    }, (error, municipios) => {
        if (error) {
            return res.status(500).send({
                message: 'Error al consultar los municipios ' + error
            });
        } else {
            if (!municipios) {
                return res.status(400).send({
                    message: 'No se encontraron municipios'
                });
            } else {
                return res.status(200).send({
                    Municipios: municipios
                });
            }
        }
    });
}
//envia el catalogo de distritos
function catalogoDistritos(req, res) {
    let params = req.body;
    let idUsuario = params.idUsuario;
    let procesoelectoral = params.procesoElectoral;

    Distritos.find({
        Activo: true
    }, (error, distritos) => {
        if (error) {
            return res.status(500).send({
                message: 'Error al obtener los distritos electorales ' + error
            });
        } else {
            if (!distritos) {
                return res.status(400).send({
                    message: 'No se encontraron distritos electorales'
                });
            } else {
                return res.status(200).send({
                    Distritos: distritos
                });
            }
        }
    }
    );
}
//envia el catalogo de secciones
function catalogoSecciones(req, res) {
    let params = req.body;
    //let idUsuario = params.idUsuario;
    Secciones.find({
        Activo: true
    }, (error, secciones) => {
        if (error) {
            return res.status(500).send({
                message: 'Error al consultar las secciones electorales ' + error
            });
        } else {
            if (!secciones) {
                return res.status(400).send({message: 'No se encontraron secciones electorales'});
            } else {
                return res.status(200).send({secciones: secciones});
            }
        }
    });
}
//envia el catalogo de tipo elecciones
function catalogoTiposEleccions(req, res) {
    let params = req.body;
    let idUsuario = params.idUsuario;
    TiposEleccion.find({
        Tipoeleccion_Activo: 1
    }, (error, tipoEleccion) => {
        if (error) {
            return res.status(500).send({
                message: 'Error al consultar los tipo de elección ' + error
            });
        } else {
            if (!tipoEleccion) {
                return res.status(400).send({
                    message: 'No se encontraron tipos de elección'
                });
            } else {
                return res.status(200).send({
                    tiposEleccion: tipoEleccion
                });
            }
        }
    });
}
//envia catalogo de casillas al app movil
function catalogoCasillas(req, res) {
    let params = req.body;
    let ProcesoElectoral_ = params.ProcesoElectoral;
    let idUsuario = params.idUsuario;
   // console.log('pp');
  //  console.log(params);
    Casillas.find({}, (error, casillas) => {
        if (error) {
        //    console.log("Error 500");
            return res.status(500).send({
                message: 'Error al consultar las casillas electorales ' + error
            });
        } else {
            if (!casillas) {
         //       console.log("Error 400");
                return res.status(400).send({
                    message: 'No se encontraron casillas'
                });
            } else {                
                let promesa= new Promise((resolve,reject)=>{
                    
                   resolve(obtenerArregloCasillas(casillas));
                });
                promesa.then((resultado)=>{
         //           console.log(resultado);
                    return res.status(200).send({Casillas:resultado});
                });
                
                
            }
        }
    });
}
async function obtenerArregloCasillas(casillas) {
    let arreglo = [];
    for (let index = 0; index < casillas.length; index++) {
        const elemento = casillas[index];            
      await arreglo.push( {_id:elemento._id,
            ProcesoElectoral:elemento.ProcesoElectoral,
            Municipio_id:elemento.Municipio,
            Distrito_id:elemento.Distrito,
            Seccion_id:elemento.Seccion,
            DescripcionCasilla:elemento.Identificacion_SIEE.DescripcionCasilla,
            Entidad_Federativa:elemento.IdentificacionCasilla.Entidad_Federativa,
            Distrito_Electoral:elemento.IdentificacionCasilla.Distrito_Electoral,
            Seccion_Electoral:elemento.IdentificacionCasilla.Seccion_Electoral,
            Tipo_Casilla:elemento.IdentificacionCasilla.Tipo_Casilla,
            Municipio:elemento.IdentificacionCasilla.Municipo});
    }
    return arreglo;
}
/*Se recibe una notificacion desde la aplicacion movil la cual modifica el estatus del 
campo sincronizacion del usuario*/
function notificacionSincronicacion(req,res){
    let params=req.body;
    let user=req.user;   
    
    UsuarioAPP.findOne({_id:user.sid},(error,encontrado)=>{
     if(error){
        return res.status(500).send({message:'Error al encontrar el usuario ' +error});
     }else{
          if(!encontrado){
            return res.status(400).send({message:'No se encontro el usuario'});
           }else{
               encontrado.Sincronizar=false;
               encontrado.save((error,guardado)=>{
                if(error){
                   return res.status(500).send({message:'Error al cambiar el estatus de la sincronización ' +error});
                }else{
                     if(!guardado){
                       return res.status(400).send({message:'no se guardo el estatus de la sincronicación'});
                      }else{
                           return res.status(200).send({Sincronizacion:true});
                      }
                }
               });                
           }
     }
    });
}
//Se obtine las casillas asignadas al usuario
function obtenerCasillausuarios(req,res){
        //registrar transaccion
        let params=req.body;
        let user=req.user;
       // console.log("Obteniendo casillas");
     //   console.log(user);
        UsuarioAPP.findById(user.sid, (error,usuario)=>{
         if(error){
            return res.status(500).send({message:'Error la obtener el usuario ' +error});
         }else{
              if(!usuario){
                return res.status(400).send({message:'No se encontro el usuario'});
               }else{
                 //   return res.status(200).send({usuario:usuario});
                var idusuario=usuario.IdUsuarioSIEAPP
            
                casillasxusuarios.find({Usuario_ID:idusuario},(error,casillas)=>{
                    if(error){
                       return res.status(500).send({message:'Error al consultar las casillas asignadas ' +error});
                    }else{
                         if(!casillas){
                           return res.status(400).send({message:'No se encontraro casillas'});
                          }else{
                            //  console.log(casillas);
                               return res.status(200).send({Casillas:casillas});
                          }
                    }
                   });

               }
         }
        })
      
}
module.exports = {
    ObtenerCasillasProceso,
    Sincronizacion,
    catalogoEleccion,
    catalogoMunicipios,
    catalogoDistritos,
    catalogoSecciones,
    catalogoTiposEleccions,
    catalogoCasillas,
    notificacionSincronicacion,
    catalogoIdentificacionActas,
    obtenerCasillausuarios
};