'use strict'

var UsuarioAPP = require('../models/usuarioApp.model');
var Movil_usuario_model = require('../models/Movil/movil_usuario.model');
var activacion_model = require('../models/activacion.model');
let Movil_informacion = require('../models/Movil/movil_informacion.model');
var jwt = require('../services/jwt');
const crypto = require('crypto');
const moment = require('moment');
let registros = require('../utilidades/registros');


var bcrypt = require('bcrypt-nodejs');
const movil_usuarioModel = require('../models/Movil/movil_usuario.model');
const { Console, log } = require('console');
//
function encriptarpass(pass) {
    let hash = crypto.createHash('sha256');
    //let cadena = hash.update(pass).digest('hex');
    let cadena = hash.digest('hex');
    return cadena;
}



//inicio de sesion del usuario
function loginUsuario(req, res) {
    let params = req.body;
    console.log(params);
    let nombre = params.nombre;
    let pass = params.password;
    let UIDD = params.UIDD;
    //   console.log(nombre);
    //  console.log(UIDD);
    if (nombre != undefined && pass != undefined) {
        //incluir validacion para el UIDD del dispositivo desde donde se esta accesando.

        if (pass.length > 0 && nombre.length > 0) {
            UsuarioAPP.findOne({
                'nombre': nombre.toLowerCase()
            }, (err, user) => {
                if (err) {
                    console.log("registro");
                    registros.registra(new Date(), nombre, "PREPCASILLA", nombre, "Login", JSON.stringify(params), "Error al realizar la peticion de acceso")
                    res.status(500).send({
                        message: 'Error en la petición de acceso'
                    });
                } else {
                    console.log('usuario app encontrado: ' + user);
                    if (!user) {
                        registros.registra(new Date(), nombre, "PREPCASILLA", nombre, "Login", JSON.stringify(params), "No se encontro al usuario app");
                        res.status(200).send({
                            message: 'El usuario no existe'
                        });
                    } else {

                        bcrypt.compare(pass, user.password, (err, check) => {
                            if (check) {


                                //222
                                Movil_informacion.findOne({
                                    'movil_informacion_Android_ID': Android_ID,
                                    'movil_informacion_activo': 1
                                }, (err, movil_informacion_entidad) => {
                                    if (err) {
                                        //console.log("registro");
                                        res.status(500).send({
                                            activacion: false,
                                            message: 'Error en la consulta del Android ID'
                                        });
                                    } else {
                                        //console.log('Folio de activacion encontrado: ' + movil_informacion_entidad);
                                        if (!movil_informacion_entidad) {
                                            res.status(200).send({
                                                activacion: false,
                                                message: 'El codigo Android ID no esta autorizado.'
                                            });
                                        }
                                        else {         
                                            
                                            
                                            registros.registra(new Date(), user._id, "PREPCASILLA", user.nombre, "Login", JSON.stringify(params), "Acceso al sistema");
                                            res.status(200).send({
                                                user: {
                                                    _id: user._id,
                                                    nombre: user.nombre,
                                                    rol: user.rol,
                                                    activo: user.activo,
                                                    estatus: user.estatus,
                                                    Sincronizar: user.Sincronizar,
                                                    Usuario_ID: user.IdUsuarioSIEAPP
                                                },
                                                token: jwt.createToken(user)
                                            });
                    
                    
                                            
                                        }
                                    }
                                });
                                //222

                                
                            } else {
                                registros.registra(new Date(), nombre, "PREPCASILLA", nombre, "Login", JSON.stringify(params), "Usuario o contraseña incorrecta")
                                res.status(404).send({
                                    message: 'El usuario no se a podido loguear'
                                })
                            }
                        });
                        // if (encriptarpass(pass) == user.password) {
                        //     registros.registra(new Date(), user._id, "PREPCASILLA", user.nombre, "Login", JSON.stringify(params), "Acceso al sistema");
                        //     res.status(200).send({
                        //         user: {
                        //             _id: user._id,
                        //             nombre: user.nombre,
                        //             rol: user.rol,
                        //             activo: user.activo,
                        //             estatus: user.estatus,
                        //             Sincronizar: user.Sincronizar,
                        //             Usuario_ID: user.IdUsuarioSIEAPP
                        //         },
                        //         token: jwt.createToken(user)
                        //     });

                        // } else {
                        //     registros.registra(new Date(), nombre, "PREPCASILLA", nombre, "Login", JSON.stringify(params), "Usuario o contraseña incorrecta")
                        //     res.status(404).send({
                        //         message: 'El usuario no se a podido loguear'
                        //     })
                        // }
                    }
                }
            });
        } else {

            res.status(400).send({
                message: 'Los campos de usuario o contraseña estan vacios'
            });
        }
    } else {
        res.status(400).send({
            message: 'Los campos de usuario o contraseña no se encuentran en la petición'
        });
    }

};

//inicio de sesion del usuario
function login_Usuario_android(req, res) {
    let params = req.body;
    
    //console.log(params);

    let nombre = params.nombre;//.toLowerCase();
    let pass = params.password;
    let Android_ID = params.android_id;
    let UIDD = params.UIDD;
    //  console.log(nombre);
    //  console.log(UIDD);
    if (nombre != undefined && pass != undefined) {
        //incluir validacion para el UIDD del dispositivo desde donde se esta accesando.

        if (pass.length > 0 && nombre.length > 0) {
            Movil_usuario_model.findOne({
                'movil_usuario_Nombre': nombre
            }, (err, user) => {
                if (err) {
                    console.log("registro");
                    registros.registra(new Date(), nombre, "PREPCASILLA", nombre, "Login", JSON.stringify(params), "Error al realizar la peticion de acceso")
                    res.status(500).send({
                        success: false,
                        message: 'Error en la petición de acceso'
                    });
                } else {
                    console.log('usuario android encontrado: ' + nombre);
                    if (!user) {
                        registros.registra(new Date(), nombre, "PREPCASILLA", nombre, "Login", JSON.stringify(params), "No se encontro al usuario android");
                        res.status(200).send({
                            success: false,
                            message: 'El usuario no existe'
                        });
                    } else {

                        if(user.movil_usuario_Activo == 0)
                        {
                            if(user.movil_usuario_Errores > 4)
                            {
                                res.status(200).send({
                                    success: false,
                                    message: 'Usuario bloqueado por multiples intento fallidos. \n\n Comuniquese al telefono: 993 265 5530'
                                })
                            }
                            else
                            {
                                res.status(200).send({
                                    success: false,
                                    message: 'Usuario inabilitado.'
                                })

                            }
                        }
                        else
                        {
                            bcrypt.compare(pass, user.movil_usuario_Contrasenia, (err, check) => {
                                if (check) {
                                    //222

                                    if(nombre != "P0001")
                                    {
                                        console.log("nombre ", nombre);
                                        Movil_informacion.findOne({
                                            'movil_informacion_Android_ID': Android_ID,
                                            'movil_informacion_activo': 1
                                        }, (err, movil_informacion_entidad) => {
                                            if (err) {
                                                //console.log("registro");
                                                res.status(500).send({
                                                    success: false,
                                                    message: 'Error en la consulta del Android ID'
                                                });
                                            } else {
                                                //console.log('Folio de activacion encontrado: ' + movil_informacion_entidad);
                                                if (!movil_informacion_entidad) {
                                                    res.status(200).send({
                                                        success: false,
                                                        message: 'El codigo Android ID no esta autorizado.'
                                                    });
                                                }
                                                else {         
                                                    
                                                    
                                                    registros.registra(new Date(), user._id, "PREPCASILLA", user.nombre, "Login", JSON.stringify(params), "Acceso al sistema");
        
                                                    Movil_usuario_model.updateOne(
                                                        {
                                                            "_id": user._id
                                                        },
                                                        {
                                                            movil_usuario_Errores: 0
                                                        },
                                                        (err, usuario) => {
                                                        });
        
                                                    res.status(200).send({
                                                        user: {
                                                            _id: user._id,
                                                            nombre: user.movil_usuario_Nombre,
                                                            rol: 'appmovil',
                                                            activo: user.movil_usuario_Activo,
                                                            estatus: 'Inicial',
                                                            Sincronizar: 1,
                                                            movil_usuario_CATD: user.movil_usuario_CATD,
                                                            movil_usuario_Movil: user.movil_usuario_Movil
                                                        },
                                                        token: jwt.createToken(user),
                                                        success: true,
                                                        message: ''
                                                    });
                            
                            
                                                    
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        console.log("nombre Play ", nombre);
                                        registros.registra(new Date(), user._id, "PREPCASILLA", user.nombre, "Login", JSON.stringify(params), "Acceso al sistema");
    
                                        Movil_usuario_model.updateOne(
                                            {
                                                "_id": user._id
                                            },
                                            {
                                                movil_usuario_Errores: 0
                                            },
                                            (err, usuario) => {
                                            });

                                            console.log("User id ", user._id);
                                            
                                        res.status(200).send({
                                            user: {
                                                _id: user._id,
                                                nombre: user.movil_usuario_Nombre,
                                                rol: 'appmovil',
                                                activo: user.movil_usuario_Activo,
                                                estatus: 'Inicial',
                                                Sincronizar: 1,
                                                movil_usuario_CATD: user.movil_usuario_CATD,
                                                movil_usuario_Movil: user.movil_usuario_Movil
                                            },
                                            token: jwt.createToken(user),
                                            success: true,
                                            message: ''
                                        });
                        
                                    }
                                    //222
    
                                    
                                } else {
                                    registros.registra(new Date(), nombre, "PREPCASILLA", nombre, "Login", JSON.stringify(params), "Usuario o contraseña incorrecta")
                                    // res.status(404).send({
                                    //     message: 'El usuario no se a podido loguear'
                                    // })
                                    console.log(user);
                                    let error_tmp = user.movil_usuario_Errores;
                                    error_tmp++;
    
                                    let movil_usuario_Activo_tmp = 1;
                                    if(error_tmp > 4)
                                        movil_usuario_Activo_tmp = 0;
    
                                    Movil_usuario_model.updateOne(
                                        {
                                            "_id": user._id
                                        },
                                        {
                                            movil_usuario_Errores: error_tmp,
                                            movil_usuario_Activo: movil_usuario_Activo_tmp
                                        },
                                        (err, usuario) => {
                                        });
    
                                    if(error_tmp < 5)
                                    {
                                        res.status(200).send({
                                            success: false,
                                            message: 'El usuario o contraseña es incorrecto'
                                        })
                                    }
                                    else
                                    {
                                        res.status(200).send({
                                            success: false,
                                            message: 'Usuario bloqueado por multiples intento fallidos.'
                                        })
                                    }
                                   
                                }
                            });
                        }

                    }
                }
            });
        } else {

            res.status(400).send({
                message: 'Los campos de usuario o contraseña estan vacios'
            });
        }
    } else {
        res.status(400).send({
            message: 'Los campos de usuario o contraseña no se encuentran en la petición'
        });
    }

};

//inicio de sesion del usuario
function prep_activacion(req, res) {
    let params = req.body;
    console.log(params);
    let codigo_activacion_cifrado = params.codigo_activacion.toUpperCase();
    let Android_ID = params.android_id;



    if (codigo_activacion_cifrado != undefined) {
        //incluir validacion para el UIDD del dispositivo desde donde se esta accesando.

        //let codigo_de_activacion = desincriptamiento_de_mensaje("13pc7",codigo_activacion_cifrado);
        let codigo_de_activacion = codigo_activacion_cifrado;
        console.log("codigo_de_activacion:", codigo_de_activacion);
        if (codigo_de_activacion.length > 0) {
            //'activacion_activo': 1
            activacion_model.findOne({
                'activacion_folio': codigo_de_activacion
                
            }, (err, activacion_entidad) => {
                if (err) {
                    //console.log("registro");
                    registros.registra(new Date(), codigo_de_activacion, "PREPCASILLA", codigo_de_activacion, "Activacion", JSON.stringify(params), "Error al realizar la peticion de Activacion")
                    res.status(500).send({
                        activacion: false,
                        message: 'Error en la activacion de la App'
                    });
                } else {
                    console.log('Folio de activacion encontrado: ' + activacion_entidad);
                    if (!activacion_entidad) {
                        registros.registra(new Date(), codigo_de_activacion, "PREPCASILLA", codigo_de_activacion, "Activacion", JSON.stringify(params), "El codigo de activacion incorrecto");
                        res.status(200).send({
                            activacion: false,
                            message: 'El código de activacion es incorrecto'
                        });
                    }
                    else {

                        if(activacion_entidad.activacion_activo == false)
                        {
                            registros.registra(new Date(), codigo_de_activacion, "PREPCASILLA", codigo_de_activacion, "Activacion", JSON.stringify(params), "El codigo de activacion ya no esta vigente");
                            res.status(200).send({
                                activacion: false,
                                message: 'El código de activacion ya no esta vigente'
                            });

                            return;
                        }

                        //222
                        if(codigo_de_activacion != 2024 && codigo_de_activacion != 'P0001')
                        {
                            //console.log("Android_ID => ", Android_ID);
                            Movil_informacion.findOne({
                                'movil_informacion_Android_ID': Android_ID,
                                'movil_informacion_activo': 1
                            }, (err, movil_informacion_entidad) => {
                                if (err) {
                                    //console.log("registro");
                                    res.status(200).send({
                                        activacion: false,
                                        message: 'Error en la consulta del Android ID'
                                    });
                                } else {
                                    //console.log('Folio de activacion encontrado: ' + movil_informacion_entidad);
                                    if (!movil_informacion_entidad) {
                                        res.status(200).send({
                                            activacion: false,
                                            message: 'El codigo Android ID no esta autorizado.'
                                        });
                                    }
                                    else {                          
                
                
                                        //111
                                        registros.registra(new Date(), activacion_entidad._id, "PREPCASILLA", codigo_de_activacion, "Activacion", JSON.stringify(params), "Activacion correcta");
                                        //Actualizamos el numero de activaciones
                                        let numero_de_activaciones_actual = (activacion_entidad.activacion_numero_actual + 1);
    
                                        console.log("numero_de_activaciones_actual " + numero_de_activaciones_actual);
                                        console.log("activacion_numero_maxima_permitido " + activacion_entidad.activacion_numero_maxima_permitido);
    
                                        let activacion_activo_tmp = 1;
                                        if (numero_de_activaciones_actual >= activacion_entidad.activacion_numero_maxima_permitido) {
                                            console.log("maximo de activacion " + activacion_activo_tmp);
                                            activacion_activo_tmp = 0;
                                        }
    
                                        activacion_model.updateOne(
                                            {
                                                "_id": activacion_entidad._id
                                            },
                                            {
                                                activacion_numero_actual: numero_de_activaciones_actual,
                                                activacion_activo: activacion_activo_tmp
                                            },
                                            (err, usuario) => {
                                            });
    
                                        let detalles_de_la_consulta = "";
                                        let movil_acceso = {};
                                        UsuarioAPP.findOne({ nombre: 'movil_prep'.toLowerCase() }, (err, usuario_entidad) => {
                                            if (err) {
                                                res.status(200).send({
                                                    activacion: false,
                                                    message: 'Error en la petición consulta de usuario de envio de actas'
                                                });
                                            } else {
                                                //Si no existe el usuario entonces lo ingresamos 
                                                console.log("user");
                                                //console.log("usuario_entidad: " + usuario_entidad);
                                                if (!usuario_entidad) {
                                                    res.status(200).send({
                                                        activacion: false,
                                                        message: 'No existe el usuario, de envio de actas'
                                                    });
                                                }
                                                else {
                                                    res.status(200).send({
                                                        activacion: true,
                                                        message: 'Activación realizada correctamente',
                                                        usuario: '$' + usuario_entidad.nombre,
                                                        acceso: usuario_entidad.password + "//13pc7"
                                                    });
                                                }
                                            }
    
                                        });
    
                                        //111
                                        
                                    }
                                }
                            });
                        }
                        else// utilizamos el codigo 2024 para la revision de play store
                        {
                            //111
                            registros.registra(new Date(), activacion_entidad._id, "PREPCASILLA", codigo_de_activacion, "Activacion", JSON.stringify(params), "Activacion correcta");
                            //Actualizamos el numero de activaciones
                            let numero_de_activaciones_actual = (activacion_entidad.activacion_numero_actual + 1);

                            console.log("numero_de_activaciones_actual " + numero_de_activaciones_actual);
                            console.log("activacion_numero_maxima_permitido " + activacion_entidad.activacion_numero_maxima_permitido);

                            let activacion_activo_tmp = 1;
                            if (numero_de_activaciones_actual >= activacion_entidad.activacion_numero_maxima_permitido) {
                                console.log("maximo de activacion " + activacion_activo_tmp);
                                activacion_activo_tmp = 0;
                            }

                            activacion_model.updateOne(
                                {
                                    "_id": activacion_entidad._id
                                },
                                {
                                    activacion_numero_actual: numero_de_activaciones_actual,
                                    activacion_activo: activacion_activo_tmp
                                },
                                (err, usuario) => {
                                });

                            let detalles_de_la_consulta = "";
                            let movil_acceso = {};
                            UsuarioAPP.findOne({ nombre: 'movil_prep'.toLowerCase() }, (err, usuario_entidad) => {
                                if (err) {
                                    res.status(200).send({
                                        activacion: false,
                                        message: 'Error en la petición consulta de usuario de envio de actas'
                                    });
                                } else {
                                    //Si no existe el usuario entonces lo ingresamos 
                                    console.log("user");
                                    //console.log("usuario_entidad: " + usuario_entidad);
                                    if (!usuario_entidad) {
                                        res.status(200).send({
                                            activacion: false,
                                            message: 'No existe el usuario, de envio de actas'
                                        });
                                    }
                                    else {
                                        res.status(200).send({
                                            activacion: true,
                                            message: 'Activación realizada correctamente',
                                            usuario: '$' + usuario_entidad.nombre,
                                            acceso: usuario_entidad.password + "//13pc7"
                                        });
                                    }
                                }

                            });

                            //111
                        }
                        //222
                        
                    }
                }
            });
        } else {

            res.status(200).send({
                activacion: false,
                message: 'El numero de activacion estan vacios'
            });
        }
    } else {
        res.status(200).send({
            activacion: false,
            message: 'El numero de activacion no se encuentran en la petición'
        });
    }

};

function guardar_informacion_de_android(req, res) {
    let entidad = req.body;
  
    console.log(entidad);
    if (entidad.movil_informacion_Android_ID != undefined) {
        
        var entidad_moviles_informacion = {

            movil_informacion_mac : entidad.movil_informacion_mac,
            movil_informacion_imei : entidad.movil_informacion_imei,
            movil_informacion_Android_ID : entidad.movil_informacion_Android_ID,
            movil_informacion_no_de_inventario : entidad.movil_informacion_no_de_inventario,
            movil_informacion_Origen : entidad.movil_informacion_Origen,
            //movil_informacion_Fecha_Registro : new Date(Date.now()),
            movil_informacion_Fecha_Registro : entidad.movil_informacion_Fecha_Registro,
            movil_informacion_activo : entidad.movil_informacion_activo
                                                  
        };

        registros.guardar_movil_informacion(entidad_moviles_informacion);


        res.status(200).send({
            success: true,
            message: 'Informacion del movil registrada.',
            entidad: entidad_moviles_informacion
        });

    } else {
        res.status(400).send({
            activacion: false,
            message: 'El ID de android, nos esta incluida en la informacion enviada'
        });
    }

};

function consultar_informacion_android(req, res) {
    let params = req.body;
    console.log(params);
    let permiso_de_consulta = params.permiso_de_consulta;
 
    if (permiso_de_consulta != undefined && permiso_de_consulta == '137_iepct') {
       
        
        if (permiso_de_consulta.length > 0) {
            Movil_informacion.find({
                //'movil_usuario_Activo': 1
            }, (err, lista_de_informacion) => {
                if (err) 
                {
                    res.status(200).send({
                        cunsulta : false,
                        message: 'Error en la consulta de informacion de android'
                    });
                } else {
                    if (!lista_de_informacion) {
                        res.status(200).send({
                            cunsulta : false,
                            message: 'No se encontro ningun resultado con la busqueda realizada'
                        });
                    } else {
                        res.status(200).send({
                            cunsulta : true,
                            message: 'Consulta realizada correctamente',
                            lista_de_informacion
                        });

                    }
                }
            });
        } else {

            res.status(400).send({
                activacion : false,
                message: 'El numero de permiso de desactivacion estan vacios'
            });
        }
    } else {
        res.status(400).send({
            activacion : false,
            message: 'El numero de permiso no se encuentran en la petición'
        });
    }

};

async function guardar_informacion_de_movil_usuario(req, res) {
    let entidad = req.body;
  
    //console.log(entidad);
    if (entidad.movil_usuario_Contrasenia != undefined) {
        
        var numero_consucutivo = 0;

        await Movil_usuario_model.find().sort({ "_id" : -1 }).limit(1)
        .then((ultimoRegistro) => {
            console.log('Último registro:', ultimoRegistro);
            numero_consucutivo = ultimoRegistro[0].movil_usuario_Consecutivo;
            numero_consucutivo ++;
            console.log('Siguiente consecutivo  1:',numero_consucutivo);
        })
        .catch((error) => {
            console.error('Error al obtener el último registro:', error);
            numero_consucutivo = 1;
        });


        Movil_usuario_model.find({
            'movil_usuario_Nombre': entidad.movil_usuario_Nombre
            //'movil_usuario_Activo': 1
        }, (err, lista_de_usuarios) => {
            if (err) 
            {
                res.status(500).send({
                    success : false,
                    message: 'Error en la consulta de usuarios'
                });
            } else {
                console.log("lista_de_usuarios", lista_de_usuarios);
                if (lista_de_usuarios.length == 0 ) {
                    
                        let movil_usuario_model = new Movil_usuario_model();
                        movil_usuario_model.movil_usuario_Nombre = entidad.movil_usuario_Nombre.toUpperCase();
                        movil_usuario_model.movil_usuario_Movil = entidad.movil_usuario_Movil;
                        movil_usuario_model.movil_usuario_CATD = entidad.movil_usuario_CATD;
                        movil_usuario_model.movil_usuario_Fecha_Registro = moment().format('YYYY-MM-DD');
                        movil_usuario_model.movil_usuario_Consecutivo = numero_consucutivo;
                        movil_usuario_model.movil_usuario_Errores = 0;
                        movil_usuario_model.movil_usuario_Activo = entidad.movil_usuario_Activo;

                        bcrypt.hash(entidad.movil_usuario_Contrasenia.toUpperCase(), null, null, function(err, hash) {

                            movil_usuario_model.movil_usuario_Contrasenia = hash;
                    
                            //registros.guardar_movil_usuario(entidad_movil_usuario);
                            //console.log(entidad_movil_usuario);
                            if (entidad.movil_usuario_Nombre != null) {

                                movil_usuario_model.save((err, movil_usuario_model) => {
                                    if (err) {
                                        return res.status(500).send({ success: false, message: 'Error al registrar el usuario ' + err});
                                    } else {
                                        if (!movil_usuario_model) {
                                            return res.status(400).send({ success: false, message: 'No se ha registrado el usuario' });
                                        } else {
                                            return res.status(200).send({ 
                                                entidad: movil_usuario_model ,
                                                message: 'Usuario registrado.',
                                                success: true,
                                            });
                                        }
                                    }
                                });
                            } else {
                                return res.status(200).send({ success: false, message: 'Introdusca el nombre del usuario' });
                            }
                        });


                } else {

                    res.status(200).send({
                        success : false,
                        message: 'El nombre de usuario, ya cuenta con un registro previo.',
                        entidad
                    });

                }
            }
        });


        

    } else {
        res.status(400).send({
            activacion: false,
            message: 'La contraseña, nos esta incluida en la informacion enviada'
        });
    }

};


async function generar_usuarios_movil_de_forma_general(req, res) {
    let entidad = req.body;
  
    console.log(entidad);

    let numero_de_usuario = entidad.numero_de_usuarios_a_generar
    let movil_usuario_CATD = 0;

    if (entidad.movil_usuario_CATD != undefined)
        movil_usuario_CATD = entidad.movil_usuario_CATD;


    var numero_consucutivo = 0;

        await movil_usuarioModel.find().sort({ "_id" : -1 }).limit(1)
        .then((ultimoRegistro) => {
            //console.log('Último registro:', ultimoRegistro);
            numero_consucutivo = ultimoRegistro[0].movil_usuario_Consecutivo;
            numero_consucutivo ++;
            console.log('Siguiente consecutivo  1:',numero_consucutivo);
        })
        .catch((error) => {
            console.error('Error al obtener el último registro:', error);
            numero_consucutivo = 1;
        });

    if (entidad.numero_de_usuarios_a_generar != undefined) {

        var lista_usuario = [];
        for (let index = 1; index <= numero_de_usuario; index++) {
            
            console.log("consecutivo", numero_consucutivo);
            var entidad_movil_usuario = {

                movil_usuario_Nombre : "P" + numero_consucutivo.toString().padStart(4, '0'),
                movil_usuario_Contrasenia : "C" + numero_consucutivo.toString().padStart(4, '0'),
                movil_usuario_Movil : 1,
                movil_usuario_CATD : movil_usuario_CATD,
                movil_usuario_Consecutivo : numero_consucutivo,
                movil_usuario_Errores : 0,
                movil_usuario_Fecha_Registro : new Date(),
                movil_usuario_Activo : 1
            };

            lista_usuario.push(entidad_movil_usuario);
            numero_consucutivo++;        
            
            //hashPassword
        }
        
        lista_usuario.forEach(element => {
            
            bcrypt.hash(element.movil_usuario_Contrasenia, null, null, function(err, hash) {
                element.movil_usuario_Contrasenia = hash;
                registros.guardar_movil_usuario(element);
            });
        });

        res.status(200).send({
            success: true,
            message: 'Usuarios registrador. Total de usuario: ' + entidad.numero_de_usuarios_a_generar ,
            //entidad: entidad.numero_de_usuarios_a_generar
        });

    } else {
        res.status(400).send({
            activacion: false,
            message: 'El numero de usuarios a generar, no esta incluida en la informacion enviada'
        });
    }

};


function actualiza_informacion_de_movil_usuario(req, res) {
    let entidad = req.body;
  
    console.log(entidad);
    if (entidad._id != undefined) {
        
        Movil_usuario_model.find({
            '_id': entidad._id
            //'movil_usuario_Activo': 1
        }, (err, lista_de_usuarios) => {
            if (err) 
            {
                res.status(500).send({
                    cunsulta : false,
                    message: 'Error en la consulta de usuarios'
                });
            } else {
                if (!lista_de_usuarios) {
                    res.status(400).send({
                        cunsulta : false,
                        message: 'No se encontro ningun resultado con la busqueda realizada'
                    });
                } else {

                    if(lista_de_usuarios.length > 0)
                    {
                        Movil_usuario_model.updateOne(
                            {
                                "_id": entidad._id
                            },
                            {
                                movil_usuario_Activo: entidad.movil_usuario_Activo
                            },
                            (err, usuario) => {
                            });
                            lista_de_usuarios[0].movil_usuario_Activo = entidad.movil_usuario_Activo;
    
                        res.status(200).send({
                            cunsulta : true,
                            message: 'Usuario actualizado',
                            lista_de_usuarios
                        });
                    }
                    else
                    {
                        res.status(200).send({
                            cunsulta : false,
                            message: 'El Id de usuario no existe',
                            lista_de_usuarios
                        });
                    }
                    

                }
            }
        });
        
    } else {
        res.status(400).send({
            activacion: false,
            message: 'La contraseña, nos esta incluida en la informacion enviada'
        });
    }

};


function consultar_usuarios_app(req, res) {
    let params = req.body;
    console.log(params);
    let permiso_de_consulta = params.permiso_de_consulta;
 
    if (permiso_de_consulta != undefined && permiso_de_consulta == '137_iepct') {
       
        
        if (permiso_de_consulta.length > 0) {
            Movil_usuario_model.find({
                //'movil_usuario_Activo': 1
            }, (err, lista_de_usuarios) => {
                if (err) 
                {
                    res.status(500).send({
                        cunsulta : false,
                        message: 'Error en la consulta de usuarios'
                    });
                } else {
                    if (!lista_de_usuarios) {
                        res.status(400).send({
                            cunsulta : false,
                            message: 'No se encontro ningun resultado con la busqueda realizada'
                        });
                    } else {
                        res.status(200).send({
                            cunsulta : true,
                            message: 'Consulta realizada correctamente',
                            lista_de_usuarios
                        });

                    }
                }
            });
        } else {

            res.status(400).send({
                activacion : false,
                message: 'El numero de permiso de desactivacion estan vacios'
            });
        }
    } else {
        res.status(400).send({
            activacion : false,
            message: 'El numero de permiso no se encuentran en la petición'
        });
    }

};

async function hashPassword(user) {
    const password = user.password;
    const saltRounds = 10;
  
    // Use a Promise to wrap bcrypt.hash for async/await
    const hashedPassword = await new Promise((resolve, reject) => {
      bcrypt.hash(password, saltRounds, (err, hash) => {
        if (err) {
          reject(err);
        } else {
          resolve(hash);
        }
      });
    });
  
    return hashedPassword;
  }


function desincriptamiento_de_mensaje(key_de_cifrado, mensaje_encriptado) {
    // we compute the sha256 of the key
    var hash = crypto.createHash("sha256");
    hash.update(key_de_cifrado, "utf8");
    var sha256key = hash.digest();

    var keyBuffer = new Buffer.from(sha256key, "utf8");

    var cipherBuffer = new Buffer.from(mensaje_encriptado, 'base64');

    console.log("cipherBuffer");
    console.log(cipherBuffer);
    console.log("----------");

    var aesDec = crypto.createDecipheriv("aes-256-ecb", keyBuffer, ''); // always use createDecipheriv when the key is passed as raw bytes
    aesDec.setAutoPadding(false);
    // console.log("aesDec");
    // console.log(aesDec);

    var output = aesDec.update(cipherBuffer);

    // Updating encrypted text
    //let decrypted = decipher.update(encryptedText);
    // let decrypted = Buffer.concat([output, aesDec.final()]);
    // return decrypted.toString();

    let descifrado = output + aesDec.final();

    let base64data = descifrado.toString('base64');
    let cadena = descifrado.toString('ascii');

    console.log("base64data");
    console.log(base64data);
    return descifrado;

    //return output + aesDec.final();
    //return "Temporal";

}

function status(req, res){
    res.status(200).send({message: 'ok'});
}


module.exports = {
    loginUsuario,
    prep_activacion,
    guardar_informacion_de_android,
    consultar_informacion_android,
    guardar_informacion_de_movil_usuario,
    generar_usuarios_movil_de_forma_general,
    actualiza_informacion_de_movil_usuario,
    consultar_usuarios_app,
    login_Usuario_android,
    status
};