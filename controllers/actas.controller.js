'use strict'

let registroActas = require('../models/Eleccion/RegistrollegadaActa.model');

function obtenerActas(req, res) {    
    registroActas.find({}, (errorC, actas) => {
        if (errorC) {
            return res.status(404).send({
                message: 'Error al procesar la peticion de actas'
            });
        } else {
            
            let resultado = {
                totalActas: 0,
                enviadasSiee: 0,
                noEnviadas: 0,
                GUB_Total:0,
                GUB_Enviadas:0,
                GUB_NO_Enviadas:0,
                GUB_Porcentaje_enviadas: 0,
                GUB_Porcentaje_Noenviadas: 0,
                actas: 0                
            };

            if (!actas) {
                return res.status(404).send(resultado);
            } else {

                const totalActas = actas.length;
                const enviadasSiee = actas.filter(acta => acta.EnviadoCorrectamente).length;    
                const GUB_Total       = actas.filter(acta => acta.Eleccion=='GUB').length;    
                const GUB_Enviadas    = actas.filter(acta => acta.EnviadoCorrectamente && acta.Eleccion=='GUB').length;    
                const GUB_NO_Enviadas = actas.filter(acta => !acta.EnviadoCorrectamente && acta.Eleccion=='GUB').length;    
                const noEnviadas = totalActas - enviadasSiee;

                resultado = {
                    totalActas: totalActas,
                    enviadasSiee: enviadasSiee,
                    noEnviadas: noEnviadas,
                    GUB_Total:GUB_Total,
                    GUB_Enviadas:GUB_Enviadas,
                    GUB_NO_Enviadas:GUB_NO_Enviadas,
                    GUB_Porcentaje_enviadas: GUB_Total>0?(GUB_Enviadas/GUB_Total)*100:0,
                    GUB_Porcentaje_Noenviadas: GUB_Total>0?(GUB_NO_Enviadas/GUB_Total)*100:0,
                    actas: actas                    
                };

                //aqui se debera filtrar la información que se manda a los dispositivos moviles y agregar la necesaria para poder operar en los dispositivos moviles.
                return res.status(200).send(resultado);
            }
        }
    });

};

 

module.exports = {
    obtenerActas
};