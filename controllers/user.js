'use strict'
var fs = require('fs');
var path = require('path');
const crypto = require('crypto');
const hash = crypto.createHash('sha256');

var bcrypt = require('bcrypt-nodejs');
var Usuario = require('../models/user');
var jwt = require('../services/jwt');
var UsuarioAPP = require('../models/usuarioApp.model');
const { log } = require('console');
let Global = require('../global');
var activacion_model = require('../models/activacion.model');
let registros = require('../utilidades/registros');



function login_usuario_siee(req, res) {
    var params = req.body;
    var nombre = params.nombre;
    var pass = params.password;
    console.log(params);
    console.log('pase por el loggin');
    Usuario.findOne({ nombre: nombre.toLowerCase() }, (err, user) => {
        console.log(user.password);
        if (err) {
            res.status(500).send({ message: 'Error en la petición de acceso' });
        } else {
            if (!user) {
                res.status(400).send({ message: 'El usuario no existe' });
            } else {
                bcrypt.compare(pass, user.password, (err, check) => {
                    if (check) {
                        if (params.gethash) {
                            res.status(200).send({
                                token: jwt.createToken(user)
                            })
                        } else {
                            res.status(200).send({ user });
                        }
                    } else {
                        res.status(404).send({ message: 'El usuario no se a podido loguear' })
                    }
                });
            }
        }
    });
};
//ALTA USUARIO
function altaUsuario(req, res) {
    var usuario = new Usuario();
    var params = req.body;
    usuario.nombre = params.dato.nombre;
    usuario.rol = params.dato.rol;
    usuario.activo = false;
    if (params.dato.password) {
        bcrypt.hash(params.dato.password, null, null, function(err, hash) {
            usuario.password = hash;
            if (usuario.nombre != null && usuario.email != null && usuario.role != null) {
                usuario.save((err, usuarioStored) => {
                    if (err) {
                        return res.status(500).send({ message: 'Error al registrar el usuario' });
                    } else {
                        if (!usuarioStored) {
                            return res.status(400).send({ message: 'No se ha registrado el usuario' });
                        } else {
                            return res.status(200).send({ usuario: usuarioStored });
                        }
                    }
                });
            } else {
                return res.status(200).send({ message: 'Introdusca todos los campos' });
            }
        });
    } else {
        return res.status(500).send({ message: 'Introdusca password' });
    }
}
//USUARIO ADMIN
function altaUsuarioIni(req, res) {
    var usuario = new Usuario();
    var params = req.body;
    usuario.nombre = 'admin';
    // usuario.email = 'juanramos@outlook.com';
    usuario.email = '123@gmail.com';
    //  usuario.password = params.dato.password;
    //usuario.password = 123;

    usuario.role = 'Role_Admin';
    usuario.image = 'Sin Imagen ....';
    usuario.persona = null;
    usuario.activo = true;

    //if (true) {
    bcrypt.hash('admin', null, null, function(err, hash) {
        //hash.update('admin');
        usuario.password = hash; //hash.digest('hex');
        console.log(usuario.password);
        if (usuario.nombre != null && usuario.email != null && usuario.role != null) {
            usuario.save((err, usuarioStored) => {
                if (err) {
                    console.log(err);
                    return res.status(500).send({ message: 'Error al registrar el usuario' });
                } else {
                    if (!usuarioStored) {
                        return res.status(400).send({ message: 'No se ha registrado el usuario' });
                    } else {
                        res.status(200).send({ usuario: usuarioStored });
                    }
                }
            });
        } else {
            return res.status(200).send({ message: 'Introdusca todos los campos' });
        }

    });
    // else {
    //     return res.status(500).send({ message: 'Introdusca password' });
    // }
};
//FUNCION DE WORD
function alta_usuario_movil(req, res) {
    let usuario = new UsuarioAPP();
    usuario.IdUsuarioSIEAPP = '1';
    usuario.nombre = 'movil_prep';
    usuario.email = '123@gmail.com';
    usuario.fecha_creacion = new Date();
    usuario.rol = 'appmovil';
    usuario.activo = true;
    usuario.estatus = 'Inicial';
    usuario.Sincronizar = true;
    var params = req.body;

    console.log('user_'+ Global.Global.movil);

    if(params.codigo_de_ingreso != 'user_'+ Global.Global.movil )
        return res.status(500).send({ message: 'No cuenta con el permiso para ingresar el usuario' });

    //primero revisamos que no exista el usauario, para poder ingresarlo
    UsuarioAPP.findOne({ nombre: usuario.nombre.toLowerCase() }, (err, user) => {
        if (err) {
            res.status(500).send({ message:  'Error en la petición de registro de usuario ' + err });
        } else {
            //Si no existe el usuario entonces lo ingresamos 
            console.log(user);
            if (!user) {
                bcrypt.hash('prep_13pc7', null, null, function(err, hash) {
                    usuario.password = hash;
                    if (usuario.nombre != null && usuario.rol != null) {
                        usuario.save((err, usuarioStored) => {
                            if (err) {
                                return res.status(500).send({ message: 'Error al registrar el usuario' });
                            } else {
                                if (!usuarioStored) {
                                    return res.status(400).send({ message: 'No se ha registrado el usuario' });
                                } else {
                                    console.log(usuarioStored);
                                    return res.status(200).send({ usuario: usuarioStored });
                                }
                            }
                        });
                    } else {
                        return res.status(200).send({ message: 'Introdusca todos los campos' });
                    }
                });
                
            } 
            else //si existe el usario, regresamos la referencia de que ya existe.
                res.status(400).send({ message: 'El usuario ya existe.' });
            
        }
    });
    

}
//FUNCION DE WORD
function eliminar_usuario_movil(req, res) {
    let params = req.body;
    console.log(params);
    let permiso_de_eliminacion = params.permiso_de_eliminacion;
    let usuario_nombre = params.usuario_nombre;
 
    if (permiso_de_eliminacion != undefined) {        
        if (permiso_de_eliminacion.length > 0) {
            UsuarioAPP.findOne({
                'nombre': usuario_nombre
            }, (err, usuario_entidad) => {
                if (err) 
                {
                    console.log("Eliminacion de usuario");
                    //registros.registra(new Date(), codigo_de_activacion, "PREPCASILLA", codigo_de_activacion, "Desactivar Folios de activacion", JSON.stringify(params), "Error al realizar la peticion de desactivar folios")
                    res.status(500).send({
                        eliminacion : false,
                        message: 'Error al eliminar fisicamente el usuario'
                    });
                } else {
                    console.log('Usuario a eliminar: ' + usuario_entidad);
                    if (!usuario_entidad) {
                        //registros.registra(new Date(), codigo_de_activacion, "PREPCASILLA", codigo_de_activacion, "Desactivar Folios de activacion", JSON.stringify(params), "El codigo de activacion es incorrecto");
                        res.status(400).send({
                            eliminacion : false,
                            message: 'No se encontro el usuario, para su eliminacion'
                        });
                    } else {
                        UsuarioAPP.findByIdAndRemove(usuario_entidad._id, (err, usuarioEliminado) => {
                            if (err) {
                                return res.status(500).send({ message: 'Error al eliminar el usuario movil' });
                            } else {
                                if (!usuarioEliminado) {
                                    return res.status(400).send({ message: 'el usuario no pudo ser eliminado' });
                                } else {
                                    res.status(200).send({ 
                                        eliminacion:true,
                                        message: 'Usuario eliminado fisicamente',
                                        usuarioEliminado });
                                }
                            }
                        });

                    }
                }
            });
        } else {

            res.status(400).send({
                activacion : false,
                message: 'El codigo de eliminacion estan vacios'
            });
        }
    } else {
        res.status(400).send({
            activacion : false,
            message: 'El numero de codigo de eliminacion no se encuentran en la petición'
        });
    }

};
//FUNCION DE WORD
function prep_ingreso_de_folios_de_activacion(req, res) {
    let params = req.body;
    console.log(params);
    let permiso_de_ingreso = params.permiso_de_ingreso;
 
    if (permiso_de_ingreso != undefined) {
        //incluir validacion para el UIDD del dispositivo desde donde se esta accesando.

        if (permiso_de_ingreso.length > 0 && permiso_de_ingreso == '13PC7_2021')
        {
            let numero_aleatorio = 0;

            let entidad_codigos = {};
            let rango_incremento = 10, rango_actual = 1;
            let codigos_ingresados_correctamente = [];
            for (let index = 0; index < 10; index++) {
                
                numero_aleatorio = generar_codigos_aleatorios_de_activacion(50000000,99999999);

                entidad_codigos = {};

                entidad_codigos.activacion_folio = numero_aleatorio;
                entidad_codigos.activacion_rango = rango_actual + '-' + (parseInt(rango_actual) +  parseInt(rango_incremento)-1);
                entidad_codigos.activacion_numero_actual = 0;
                entidad_codigos.activacion_numero_maxima_permitido = 2;
                entidad_codigos.activacion_fecha_creacion = new Date();
                entidad_codigos.activacion_activo = 1;

                registros.activacion_registro(entidad_codigos);

                codigos_ingresados_correctamente.push(entidad_codigos);

                rango_actual += rango_incremento;
                
            }


            res.status(400).send({
                success : true,
                codigos_ingresados_correctamente
            });
            
        } 
        else 
        {
            res.status(400).send({
                success : false,
                message: 'El permiso de ingresoestan vacios'
            });
        }
    } else {
        res.status(400).send({
            success : false,
            message: 'El permiso de ingreso no se encuentran en la petición'
        });
    }

};

function generar_codigos_aleatorios_de_activacion(min, max) 
{
    return Math.floor((Math.random() * (max - min + 1)) + min);
};

async function guardar_folios_de_activacion(req, res) {
    let entidad = req.body;
  
    console.log(entidad);
    if (entidad.activacion_folio  != undefined) {
        
        var numero_consucutivo = 0;
    
        if (entidad._id  == undefined) 
        {
            console.log("undefined");
            activacion_model.find({
                'activacion_folio': entidad.activacion_folio
                //'movil_usuario_Activo': 1
            }, (err, lista_activacion) => {
                if (err) 
                {
                    res.status(500).send({
                        success : false,
                        message: 'Error en la consulta de codigos'
                    });
                } else {
                    console.log("lista_de_codigos", lista_activacion);
                    if (lista_activacion.length == 0 ) {
                        
                            let activacion_model_tmp = new activacion_model();
    
    
    
                            activacion_model_tmp.activacion_folio = entidad.activacion_folio;
                            activacion_model_tmp.activacion_rango = 1;
                            activacion_model_tmp.activacion_numero_actual = 0;
                            activacion_model_tmp.activacion_numero_maxima_permitido = entidad.activacion_numero_maxima_permitido;
                            activacion_model_tmp.activacion_fecha_creacion = new Date();
                            activacion_model_tmp.activacion_activo = 1;
                           
                            activacion_model_tmp.save((err, activacion_model_tmp) => {
                                if (err) {
                                    return res.status(500).send({ success: false, message: 'Error al registrar el codigo ' + err});
                                } else {
                                    if (!activacion_model_tmp) {
                                        return res.status(400).send({ success: false, message: 'No se ha registrado el codigo' });
                                    } else {
                                        return res.status(200).send({ 
                                            entidad: activacion_model_tmp ,
                                            message: 'Codigo registrado.',
                                            success: true,
                                        });
                                    }
                                }
                            });
                            
    
    
                    } else {
    
                        res.status(200).send({
                            success : false,
                            message: 'El codigo ya cuenta con un registro previo.',
                            entidad
                        });
    
                    }
                }
            });
        }
        else
        {
            console.log("_ID", );
            activacion_model.find({
                '_id': entidad._id
                //'movil_usuario_Activo': 1
            }, (err, lista_activacion) => {
                if (err) 
                {
                    res.status(500).send({
                        success : false,
                        message: 'Error en la consulta de codigos'
                    });
                } else {
                    console.log("lista_de_codigos", lista_activacion);
                    if (lista_activacion.length >  0 ) {
                        
                            let activacion_model_tmp = new activacion_model();
    
    

                            activacion_model.updateOne(
                                {
                                    "_id": entidad._id
                                },
                                {
                                    activacion_folio: entidad.activacion_folio,
                                    activacion_numero_maxima_permitido: entidad.activacion_numero_maxima_permitido,
                                    activacion_activo: entidad.activacion_activo
                                },
                                (err, usuario) => {
                                });
                                lista_activacion[0].activacion_folio = entidad.activacion_folio;
                                lista_activacion[0].activacion_numero_maxima_permitido = entidad.activacion_numero_maxima_permitido;
                                lista_activacion[0].activacion_activo = entidad.activacion_activo;
        
                            res.status(200).send({
                                cunsulta : true,
                                message: 'Folio actualizado',
                                lista_activacion
                            });

    
                    } else {
    
                        res.status(200).send({
                            success : false,
                            message: 'El codigo ya cuenta con un registro previo.',
                            entidad
                        });
    
                    }
                }
            });
        }
        


        

    } else {
        res.status(400).send({
            activacion: false,
            message: 'La contraseña, nos esta incluida en la informacion enviada'
        });
    }

};


function actualiza_folios_de_activacion(req, res) {
    let entidad = req.body;
  
    //console.log(entidad);
    if (entidad._id != undefined) {
        
        activacion_model.find({
            '_id': entidad._id
            //'movil_usuario_Activo': 1
        }, (err, lista_de_codigo) => {
            if (err) 
            {
                res.status(500).send({
                    cunsulta : false,
                    message: 'Error al consultas los codigos' + err
                });
            } else {
                if (!lista_de_codigo) {
                    res.status(400).send({
                        cunsulta : false,
                        message: 'No se encontro el codigo de activacion'
                    });
                } else {

                    activacion_model.updateOne(
                        {
                            "_id": entidad._id
                        },
                        {
                            activacion_folio: entidad.activacion_folio,
                            activacion_numero_maxima_permitido: entidad.activacion_numero_maxima_permitido,
                            activacion_activo: entidad.activacion_activo
                        },
                        (err, usuario) => {
                        });
                        lista_de_codigo[0].activacion_folio = entidad.activacion_folio;
                        lista_de_codigo[0].activacion_numero_maxima_permitido = entidad.activacion_numero_maxima_permitido;
                        lista_de_codigo[0].activacion_activo = entidad.activacion_activo;

                    res.status(200).send({
                        cunsulta : true,
                        message: 'Folio actualizado',
                        lista_de_codigo
                    });

                }
            }
        });
        
    } else {
        res.status(400).send({
            activacion: false,
            message: 'El ID, nos esta incluida en la informacion enviada'
        });
    }

};


//FUNCION DE WORD
function desactivar_folios_de_activacion(req, res) {
    let params = req.body;
    console.log(params);
    let codigo_de_activacion = params.codigo_activacion;
 
    if (codigo_de_activacion != undefined) {
       
        
        if (codigo_de_activacion.length > 0) {
            activacion_model.findOne({
                'activacion_folio': codigo_de_activacion,
                'activacion_activo': 1
            }, (err, activacion_entidad) => {
                if (err) 
                {
                    console.log("registro");
                    //registros.registra(new Date(), codigo_de_activacion, "PREPCASILLA", codigo_de_activacion, "Desactivar Folios de activacion", JSON.stringify(params), "Error al realizar la peticion de desactivar folios")
                    res.status(500).send({
                        activacion : false,
                        message: 'Error en la desactivacion de los folios'
                    });
                } else {
                    console.log('Activacion encontrado: ' + activacion_entidad);
                    if (!activacion_entidad) {
                        //registros.registra(new Date(), codigo_de_activacion, "PREPCASILLA", codigo_de_activacion, "Desactivar Folios de activacion", JSON.stringify(params), "El codigo de activacion es incorrecto");
                        res.status(400).send({
                            activacion : false,
                            message: 'El codigo a desactivar utilizado no existe'
                        });
                    } else {

                        //registros.registra(new Date(), activacion_entidad._id, "PREPCASILLA", codigo_de_activacion, "Desactivar Folios de activacion", JSON.stringify(params), "Codigo desactivado correcta");
                        //Actualizamos el numero de activaciones
                        let numero_de_activaciones_actual = (activacion_entidad.activacion_numero_actual + 1);
                        activacion_model.updateOne({"_id":activacion_entidad._id}, { activacion_activo: 0 }, (err, usuario) => {
                            res.status(200).send({
                                activacion : true,
                                message: 'El código se ha desactivado correctamente'
                            });
                        });

                    }
                }
            });
        } else {

            res.status(400).send({
                activacion : false,
                message: 'El numero de activacion estan vacios'
            });
        }
    } else {
        res.status(400).send({
            activacion : false,
            message: 'El numero de activacion no se encuentran en la petición'
        });
    }

};
//FUNCION DE WORD
function consultar_folios_de_activacion(req, res) {
    let params = req.body;
    console.log(params);
    let permiso_de_consulta = params.permiso_de_consulta;
 
    if (permiso_de_consulta != undefined && permiso_de_consulta == '137_iepct') {
        //'activacion_activo': 1
        if (permiso_de_consulta.length > 0) {
            activacion_model.find({
            }, (err, lista_de_folio) => {
                if (err) 
                {
                    res.status(500).send({
                        cunsulta : false,
                        message: 'Error en la consulta de los folios'
                    });
                } else {
                    if (!lista_de_folio) {
                        res.status(400).send({
                            cunsulta : false,
                            message: 'No se encontro ningun resultado con la busqueda realizada'
                        });
                    } else {
                        res.status(200).send({
                            cunsulta : true,
                            message: 'Consulta realizada correctamente',
                            lista_de_folio
                        });

                    }
                }
            });
        } else {

            res.status(400).send({
                activacion : false,
                message: 'El numero de permiso de desactivacion estan vacios'
            });
        }
    } else {
        res.status(400).send({
            activacion : false,
            message: 'El numero de permiso no se encuentran en la petición'
        });
    }

};


function loginUsuario(req, res) {
    var params = req.body;
    var nombre = params.nombre;
    var pass = params.password;
    console.log(params);
    console.log('pase por el loggin');
    Usuario.findOne({ nombre: nombre.toLowerCase() }, (err, user) => {
        console.log(user.password);
        if (err) {
            res.status(500).send({ message: 'Error en la petición de acceso' });
        } else {
            if (!user) {
                res.status(400).send({ message: 'El usuario no existe' });
            } else {
                bcrypt.compare(pass, user.password, (err, check) => {
                    if (check) {
                        if (params.gethash) {
                            res.status(200).send({
                                token: jwt.createToken(user)
                            })
                        } else {
                            res.status(200).send({ user });
                        }
                    } else {
                        res.status(404).send({ message: 'El usuario no se a podido loguear' })
                    }
                });
            }
        }
    });
};



module.exports = {
    login_usuario_siee,
    altaUsuario,
    loginUsuario,
    altaUsuarioIni,
    alta_usuario_movil,
    eliminar_usuario_movil,
    prep_ingreso_de_folios_de_activacion,
    desactivar_folios_de_activacion,
    consultar_folios_de_activacion,
    guardar_folios_de_activacion,
    actualiza_folios_de_activacion
};