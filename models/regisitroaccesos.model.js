'use strict'
let mongoose=require('mongoose');
let appUserRegistroSchema=new mongoose.Schema({        
            horaRegistro:Date,
            idUsuario:String,
            Aplicacion:String,
            NombreUsuario:String,
            operacion:String,//login,cerrar sesión,obtencion de catalogo casillas, subida de actas
            parametros:String,
            resultados:String
});
module.exports=mongoose.model('appUserRegistro',appUserRegistroSchema);