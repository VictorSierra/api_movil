var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var MovilesSchema = Schema({

    movil_usuario_Nombre: String,
    movil_usuario_Contrasenia: String,
    movil_usuario_Movil: String,
    movil_usuario_CATD: String,
    movil_usuario_Fecha_Registro: Date,
    movil_usuario_Consecutivo: Number,
    movil_usuario_Errores: Number ,
    movil_usuario_Activo: Boolean,    
    
});

module.exports = mongoose.model('movil_usuario', MovilesSchema);