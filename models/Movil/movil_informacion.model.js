var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var MovilesSchema = Schema({


    movil_informacion_mac: String,
    movil_informacion_imei: String,
    movil_informacion_Android_ID: String,
    movil_informacion_no_de_inventario: String,
    movil_informacion_Origen: String,
    movil_informacion_Fecha_Registro: Date,
    movil_informacion_activo: Boolean,    
});

module.exports = mongoose.model('movil_informacion', MovilesSchema);