'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var CasillasSchema = Schema({
    ProcesoElectoral:{ type: Schema.Types.ObjectId, ref: 'procesoelectorals' },
    IdentificacionCasilla:{
        Entidad_Federativa:Number,
        Distrito_Electoral:Number,
        Seccion_Electoral:Number,
        Tipo_Casilla:String,        
        Municipo:Number
    }, 
    Municipio:{ type: Schema.Types.ObjectId, ref: 'municipios',index:true },       
    Distrito:{type:Schema.Types.ObjectId,ref:'distritos', index:true },
    Seccion:{type:Schema.Types.ObjectId,ref:'seccions', index:true },
    Identificacion_SIEE:{
        SeccionElectoral_ID:Number,
        SeccionElectoral_Numero:Number,
        DistritoElectoral_ID:Number,
        DescripcionCasilla:String,  
        Municipio_Numero:Number,
        MunicipioElectoral_ID:Number,
        ClaveCasilla:String
    }   
},{ usePushEach: true });

module.exports = mongoose.model('Casilla', CasillasSchema);