'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var APPUsersSchema = Schema({
    nombre: String,
    email:String,
    password: String,
    fecha_creacion:Date,
    rol: String, 
    activo: Boolean,
    activoSIEE:Boolean,
    IdUsuarioSIEAPP:Number,
    codigoImei:String,
    UIDD:String,
    estatus:String,
    Sincronizar:Boolean
});

module.exports = mongoose.model('APPUser', APPUsersSchema);