'use strict'
let mongoose=require('mongoose');
let Schema=mongoose.Schema;
let TipoEleccionSchema= Schema({
    TipoEleccion_ID:Number,
    Nombre:String,
    Abreviacion:String,
    Tipoeleccion_Activo:Number,
    ProcesoElectoral_ID:String
});
module.exports=mongoose.model('TipoEleccion',TipoEleccionSchema);