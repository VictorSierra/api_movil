'use strict'
let mongoose=require('mongoose');
let Schema= mongoose.Schema;
let DistritoElectoralSchema=Schema({
    DistritoElectoral_ID:Number,
    Numero:Number,
    Descripcion:String,
    Abreviacion:String,
    Activo:Boolean,
    MunicipioElectoral_ID:Number,
    Municipio:{ type: Schema.Types.ObjectId, ref: 'municipios', index:true },
    Secciones:[{
        _id:{ type: Schema.Types.ObjectId, ref: 'seccions',index:true }
    }]    
},{ usePushEach: true });

module.exports = mongoose.model('Distrito', DistritoElectoralSchema);