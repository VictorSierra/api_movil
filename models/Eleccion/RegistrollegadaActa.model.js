'use strict'
let mongoose=require('mongoose');
let Schema=mongoose.Schema;
let RegistroActasSchema=Schema({
     
//MODELO QUE SE MUESTRA EN MONGO    
    Eleccion:String,      
    Ambito:String,
    Seccion:String,
    Casilla:String,
    QRActa:String,                                
    ProcesoElectoral:String,                                    
    HoraProcesadoServidor:Date,
    HoraCaptura:String,
    FechaAcopio:Date,
    HoraAcopio:String,
    Usuario:String,
    Hash:String,
    NombreArchivoRepositorio:String,
    ArchivoOrigen:String,
    Ubicacion:String,
    Imei:String,
    EnviadoCorrectamente:Boolean,
    MsgError:String,
    ApiRecibe:String,
    CasillaNombre:String,
    EleccionNombre:String,
    AmbitoNombre:String,
    Fecharegistro:Date,
});

module.exports=mongoose.model('RegistroRecepcionActas',RegistroActasSchema);