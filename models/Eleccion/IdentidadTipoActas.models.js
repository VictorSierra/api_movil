'use strict'
let mongoose=require('mongoose');
let Schema=mongoose.Schema;
let identidadTipoActasSchema= Schema({
    TipoEleccion_ID:Number,
    TipoCasilla_ID:Number,
    FoliacionActa_Numero:Number,    
    ProcesoElectoral_ID:String
});
module.exports=mongoose.model('IdentidadTipoActas',identidadTipoActasSchema);