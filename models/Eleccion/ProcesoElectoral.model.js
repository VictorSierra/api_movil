'use strict'
let mongoose=require('mongoose');
let Schema=mongoose.Schema;
let ProcesoElectoralSchema=Schema({
    ProcesoElectoralID:String,
    TipoProcesoElectoral:String,
    TipoProcesoElectoralAbreviacion:String,
    TipoProcesoElectoral_ID:Number,
    FechaJornada:Date,
    FechaCierre:Date,
    NombreProcesoElectoral:String,
    Activo:Boolean,
    Distritos:[{
        IdDistrito:{ type: Schema.Types.ObjectId, ref: 'distritos', index:true }
    }]    
},{ usePushEach: true });
module.exports=mongoose.model('ProcesoElectoral',ProcesoElectoralSchema);