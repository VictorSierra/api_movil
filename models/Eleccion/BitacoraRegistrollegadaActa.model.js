'use strict'
let mongoose=require('mongoose');
let Schema=mongoose.Schema;
let BitacoraRegistroActasSchema=Schema({
     
//MODELO QUE SE MUESTRA EN MONGO    
    RegistroRecepcionActas_ID:String,      
    FechaRegistro:Date,
    EnviadoCorrectamente:Boolean,
    MsgError:String
});

module.exports=mongoose.model('BitacoraRegistrollegadaActa',BitacoraRegistroActasSchema);