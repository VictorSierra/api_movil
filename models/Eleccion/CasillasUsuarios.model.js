  'use strict'
let mongoose=require('mongoose');
let Schema= mongoose.Schema;
let CasillaXUsuarioSchema=Schema({
    ProcesoElectoral_ID:String,
    Casilla_Descripcion:String,
    Casilla_Clave:String,
    SeccionElectoral_Numero:String,
    DistritoElectoral_ID:String,
    MunicipioElectoral_ID:String,
    TipoEleccion_ID:String,
    FoliacionActa_Numero:String,
    Eleccion_ID:String,
    Usuario_ID:String
});

module.exports = mongoose.model('CasillasxUsuario', CasillaXUsuarioSchema);