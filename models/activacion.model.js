'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var APPActivacionSchema = Schema({
    activacion_folio: String,
    activacion_rango:String,
    activacion_numero_actual:Number,
    activacion_numero_maxima_permitido: Number, 
    activacion_fecha_creacion: Date,
    activacion_activo: Boolean
});
module.exports = mongoose.model('APPActivacion', APPActivacionSchema);