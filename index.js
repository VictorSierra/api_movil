'use strict'

var Global = require('./global');
//var assert = require('assert');
var mongoose = require('mongoose');
var app = require('./app');

var port = process.env.PORT || 3000
mongoose.connect(Global.Global.urlBase, { useMongoClient: true, autoIndex: false });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error',
    console.error.bind(console, 'error de conexion')
);
db.once('open', function() {
    console.log('la base de datos se conecto correctamente...');
    app.listen(port, function() {
        console.log('Servidor de api rest escuchando en el puerto: ' + port);
    });
});