'use strict'
var express = require('express');
var bodyParser = require('body-parser');
var helmet = require('helmet')

var app = express();

//carga rutas
var user_routes = require('./routes/user');
var catalogo_routes = require('./routes/catalogos');
app.use(helmet());

// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));


//configuracion de cabeceras http   
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'authorization,X-API-KEY,Origin,X-Requested-With,Content-Type,Accept,Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET,POST');
    res.header('Allow', 'GET', 'POST');
    res.header()
    next();
});
//rutas base
app.use('/api', user_routes);
// app.use('/api', catalogo_routes);
app.use(express.static('apks'));

module.exports = app;