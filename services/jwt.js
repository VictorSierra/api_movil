'user strict'
let jwt = require('jwt-simple');
let moment = require('moment');

let secret = 'fskjfhsdfkljhsdfklhfkljhsfdibyrbryburbcllwec';
exports.createToken = function(user) {
    let payload = {
        sid: user._id,
        nombre: user.nombre,
        email: user.email,
        fecha_creacion: user.fecha_creacion,
        role: user.role,
        iat: moment().format(),
        exp: moment().add(1, 'd').format()
    };
    return jwt.encode(payload, secret);
}